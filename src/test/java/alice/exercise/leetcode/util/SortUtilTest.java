package alice.exercise.leetcode.util;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Alice
 * @since 2023-02-28 星期二 14:30
 */
public class SortUtilTest extends TestCase {

  private static final List<Integer> LIST;

  static {
    LIST = new ArrayList<>();
    for (int i = 0; i < 20; i++) {
      LIST.add(i);
    }
  }

  public void testBubbleSort() {
    int[] aCase = getCase();
    SortUtil.bubbleSort(aCase);
    System.out.println(Arrays.toString(aCase));
  }

  public void testQuickSort() {
    int[] aCase = getCase();
    System.out.println("aCase = " + Arrays.toString(aCase));
    SortUtil.quickSort(aCase);
    System.out.println(Arrays.toString(aCase));
  }

  private int[] getCase() {
    Collections.shuffle(LIST);
    int size = LIST.size();
    int[] result = new int[size];
    for (int i = 0; i < size; i++) {
      result[i] = LIST.get(i);
    }
    return result;
  }
}