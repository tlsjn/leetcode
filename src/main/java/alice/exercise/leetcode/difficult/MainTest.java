package alice.exercise.leetcode.difficult;

import org.junit.Test;

import java.util.Objects;

import static alice.exercise.leetcode.util.SolutionUtil.loadArray;

/**
 * @author Alice
 * @since 2023-02-06 星期一 16:58
 */
public class MainTest {
  private static final Solution SOLUTION = new Solution();

  @Test
  public void testStreamChecker() {
    // [null,false,false,false,false,false,
    // true,true,true,true,true,
    // false,false,
    // true,true,true,true,
    // false,false,false,
    // true,true,true,true,true,true,
    // false,
    // true,true,true,
    // false]
    // StreamChecker checker = new StreamChecker(new String[]{"cd", "f", "kl"});
    // for (int i = 'a'; i < 'm'; i++) {
    //   System.out.println((char) i + ": " + checker.query((char) i));
    // }
    // ["StreamChecker","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query"]
    // [[[]],["a"],["a"],["a"],["a"],["a"],["b"],["a"],["b"],["a"],["b"],["b"],["b"],["a"],["b"],["a"],["b"],["b"],["b"],["b"],["a"],["b"],["a"],["b"],["a"],["a"],["a"],["b"],["a"],["a"],["a"]]
    StreamChecker checker1 = new StreamChecker(new String[]{"ab", "ba", "aaab", "abab", "baa"});
    for (int i = 0; i < 5; i++) {
      System.out.println(checker1.query('a'));
    }
    System.out.println(checker1.query('b'));
    System.out.println(checker1.query('a'));
    System.out.println(checker1.query('b'));
    System.out.println(checker1.query('a'));
    for (int i = 0; i < 3; i++) {
      System.out.println(checker1.query('b'));
    }
    System.out.println(checker1.query('a'));
    System.out.println(checker1.query('b'));
    System.out.println(checker1.query('a'));
    for (int i = 0; i < 4; i++) {
      System.out.println(checker1.query('b'));
    }
    System.out.println(checker1.query('a'));
    System.out.println(checker1.query('b'));
    System.out.println(checker1.query('a'));
    System.out.println(checker1.query('b'));
    for (int i = 0; i < 3; i++) {
      System.out.println(checker1.query('a'));
    }
    System.out.println(checker1.query('b'));
    for (int i = 0; i < 3; i++) {
      System.out.println(checker1.query('a'));
    }
  }

  @Test
  public void test_numDupDigitsAtMostN() {
    System.out.println(SOLUTION.numDupDigitsAtMostN(101));//11
    // System.out.println(SOLUTION.numDupDigitsAtMostN(20));// 1
    // System.out.println(SOLUTION.numDupDigitsAtMostN(100));// 10
    // System.out.println(SOLUTION.numDupDigitsAtMostN(1000));// 262
    // System.out.println(SOLUTION.numDupDigitsAtMostN(1000000000));// 994388230
  }

  @Test
  public void test_countSubarrays() {
    System.out.println(SOLUTION.countSubarrays(new int[]{4, 1, 3, 2}, 1));
    System.out.println(SOLUTION.countSubarrays(Objects.requireNonNull(loadArray("2488/413879154.txt")), 7378));
    System.out.println(SOLUTION.countSubarrays(new int[]{10, 3, 8, 5, 6, 7, 2, 9, 4, 1}, 9));
    System.out.println(SOLUTION.countSubarrays(new int[]{3, 2, 1, 4, 5}, 4));
    System.out.println(SOLUTION.countSubarrays(new int[]{2, 3, 1}, 3));
  }

  @Test
  public void test_countTriplets() {
    System.out.println(SOLUTION.countTriplets(new int[]{2, 1, 3}));
    System.out.println(SOLUTION.countTriplets(new int[]{0, 0, 0}));
  }

  @Test
  public void test_minTaps() {
    System.out.println(SOLUTION.minTaps(5, new int[]{3, 4, 1, 1, 0, 0}));
    System.out.println(SOLUTION.minTaps(3, new int[]{0, 0, 0, 0}));
  }

  @Test
  public void test_isGoodArray() {
    System.out.println(SOLUTION.isGoodArray(new int[]{12, 5, 7, 23}));
    System.out.println(SOLUTION.isGoodArray(new int[]{29, 6, 10}));
    System.out.println(SOLUTION.isGoodArray(new int[]{3, 6}));
  }

  @Test
  public void test_gcd() {
    System.out.println(SOLUTION.gcd(1, 1));
    System.out.println(SOLUTION.gcd(2, 1));
    System.out.println(SOLUTION.gcd(4, 2));
    System.out.println(SOLUTION.gcd(3, 4));
    System.out.println(SOLUTION.gcd(9, 15));
  }

  @Test
  public void test_overCount() {
    for (int i = 2; i <= 4; i++) {
      System.out.println("SOLUTION.overCount(4, " + i + ") = " + SOLUTION.overCount(4, i));
    }
  }

  @Test
  public void test_dieSimulator() {
    System.out.println(SOLUTION.dieSimulator(4, new int[]{2, 1, 1, 3, 3, 2}));
    System.out.println(SOLUTION.dieSimulator(2, new int[]{1, 1, 2, 2, 2, 3}));
    System.out.println(SOLUTION.dieSimulator(2, new int[]{1, 1, 1, 1, 1, 1}));
    System.out.println(SOLUTION.dieSimulator(3, new int[]{1, 1, 1, 2, 2, 3}));
  }

  @Test
  public void test_MKAverage_1() {
    MKAverage obj = new MKAverage(3, 1);
    obj.addElement(17612);
    obj.addElement(74607);
    System.out.println(obj.calculateMKAverage());
    obj.addElement(8272);
    obj.addElement(33433);
    System.out.println(obj.calculateMKAverage());
    obj.addElement(15456);
    obj.addElement(64938);
    System.out.println(obj.calculateMKAverage());
    obj.addElement(99741);
  }

  @Test
  public void test_MKAverage() {
    MKAverage obj = new MKAverage(3, 1);
    obj.addElement(3);
    obj.addElement(1);
    System.out.println(obj.calculateMKAverage());
    obj.addElement(10);
    System.out.println(obj.calculateMKAverage());
    obj.addElement(5);
    obj.addElement(5);
    obj.addElement(5);
    System.out.println(obj.calculateMKAverage());
  }

  @Test
  public void test_rectangleArea() {
    int[] arr1 = {0, 0, 2, 2};
    int[] arr2 = {1, 0, 2, 3};
    int[] arr3 = {1, 0, 3, 1};
    // System.out.println(rectangleArea(new int[][]{arr1, arr2, arr3}));
    System.out.println(SOLUTION.rectangleArea(new int[][]{new int[]{0, 0, 1000000000, 1000000000}}));
  }
}
