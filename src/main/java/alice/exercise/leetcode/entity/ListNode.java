package alice.exercise.leetcode.entity;

import java.util.StringJoiner;

/**
 * @author Alice
 * @since 2023-02-06 星期一 9:25
 */
public class ListNode {
  public int val;
  public ListNode next;

  public ListNode() {
  }

  public ListNode(int val) {
    this.val = val;
  }

  public ListNode(int val, ListNode next) {
    this.val = val;
    this.next = next;
  }

  public static ListNode of(int... values) {
    int len = values.length;
    if (len == 0) {
      return null;
    }
    ListNode head = new ListNode(values[0]);
    ListNode t = head;
    for (int i = 1; i < len; i++) {
      t.next = new ListNode(values[i]);
      t = t.next;
    }
    return head;
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(", ", "[", "]");
    ListNode tmp = this;
    while (tmp != null) {
      joiner.add("" + tmp.val);
      tmp = tmp.next;
    }
    return joiner.toString();
  }
}
