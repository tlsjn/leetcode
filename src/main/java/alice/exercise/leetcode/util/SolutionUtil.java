package alice.exercise.leetcode.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alice
 * @since 2023-02-06 星期一 10:16
 */
public class SolutionUtil {
  public static List<List<String>> buildList(String... args) {
    List<List<String>> list = new ArrayList<>();
    for (int i = 0; i < args.length; i += 2) {
      List<String> l = new ArrayList<>();
      l.add(args[i]);
      l.add(args[i + 1]);
      list.add(l);
    }
    return list;
  }

  public static int[][] buildArray(int... args) {
    int len = ((args.length + 1) >> 1);
    int[][] list = new int[len][2];
    for (int i = 0; i < args.length; i += 2) {
      int[] row = new int[2];
      row[0] = args[i];
      row[1] = args[i + 1];
      list[i >> 1] = row;
    }
    return list;
  }

  public static String b(int n) {
    return b(n, -1);
  }

  public static String b(int n, int bits) {
    StringBuilder builder = new StringBuilder().append(n).append(": ");
    String s = Integer.toBinaryString(n);
    builder.append("0".repeat(Math.max(bits - s.length(), 0)));
    return builder.append(s).toString();
  }

  public static int[] loadArray(String path) {
    File file = new File("target/classes/cases/" + path);
    try (
      FileInputStream fileInputStream = new FileInputStream(file);
      BufferedInputStream buffered = new BufferedInputStream(fileInputStream)
    ) {
      String[] split = new String(buffered.readAllBytes()).split(",");
      int[] arr = new int[split.length];
      for (int i = 0; i < split.length; i++) {
        arr[i] = Integer.parseInt(split[i].trim());
      }
      return arr;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
