package alice.exercise.leetcode.middle;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static alice.exercise.leetcode.util.SolutionUtil.b;
import static alice.exercise.leetcode.util.SolutionUtil.buildArray;

/**
 * @author Alice
 * @since 2023-02-06 星期一 16:07
 */
public class MainTest {
  private static final Solution SOLUTION = new Solution();

  @Test
  public void test_isRobotBounded() {
    System.out.println(SOLUTION.isRobotBounded("GLGLGGLGL"));
    System.out.println(SOLUTION.isRobotBounded("GGLLGG"));
    System.out.println(SOLUTION.isRobotBounded("GG"));
    System.out.println(SOLUTION.isRobotBounded("GL"));
  }

  @Test
  public void test_maxWidthOfVerticalArea() {
    System.out.println(SOLUTION.maxWidthOfVerticalArea(buildArray(8, 7, 9, 9, 7, 4, 9, 7)));
    System.out.println(SOLUTION.maxWidthOfVerticalArea(buildArray(3, 1, 9, 0, 1, 0, 1, 4, 5, 3, 8, 8)));
  }

  @Test
  public void test_countVowelStrings() {
    for (int i = 0; i < 50; i++) {
      System.out.println("f(" + i + ") = " + SOLUTION.countVowelStrings(i));
    }
  }

  @Test
  public void test_findLengthOfShortestSubarray() {
    System.out.println(SOLUTION.findLengthOfShortestSubarray(new int[]{1, 2, 3, 10, 4, 2, 3, 5}));
    System.out.println(SOLUTION.findLengthOfShortestSubarray(new int[]{5, 4, 3, 2, 1}));
    System.out.println(SOLUTION.findLengthOfShortestSubarray(new int[]{1, 2, 3}));
    System.out.println(SOLUTION.findLengthOfShortestSubarray(new int[]{1}));
  }

  @Test
  public void test_checkArithmeticSubarrays() {
    System.out.println(SOLUTION.checkArithmeticSubarrays(new int[]{1, 2, 10, -6, -7, 8, 16, 0, 0, 10, 20, 15, -2, -3, -1, -4, -4, -8, -2}, new int[]{14, 5, 11, 15, 12, 13, 9, 7, 0}, new int[]{15, 8, 14, 18, 15, 16, 12, 8, 1}));
    System.out.println(SOLUTION.checkArithmeticSubarrays(new int[]{4, 6, 5, 9, 3, 7}, new int[]{0, 0, 2}, new int[]{2, 3, 5}));
    System.out.println(SOLUTION.checkArithmeticSubarrays(new int[]{-12, -9, -3, -12, -6, 15, 20, -25, -20, -15, -10}, new int[]{0, 1, 6, 4, 8, 7}, new int[]{4, 4, 9, 7, 9, 10}));
  }

  @Test
  public void test_bestTeamScore() {
    System.out.println(SOLUTION.bestTeamScore(new int[]{1, 3, 5, 10, 15}, new int[]{1, 2, 3, 4, 5}));
    System.out.println(SOLUTION.bestTeamScore(new int[]{4, 5, 6, 5}, new int[]{2, 1, 2, 1}));
    System.out.println(SOLUTION.bestTeamScore(new int[]{1, 2, 3, 5}, new int[]{8, 9, 10, 1}));
  }

  @Test
  public void test_checkPalindromeFormation() {
    System.out.println(SOLUTION.checkPalindromeFormation("aejbaalflrmkswrydwdkdwdyrwskmrlfqizjezd", "uvebspqckawkhbrtlqwblfwzfptanhiglaabjea"));
    System.out.println(SOLUTION.checkPalindromeFormation("x", "y"));
    System.out.println(SOLUTION.checkPalindromeFormation("abdef", "fecab"));
    System.out.println(SOLUTION.checkPalindromeFormation("ulacfd", "jizalu"));
  }

  @Test
  public void test_maximalNetworkRank() {
    System.out.println(SOLUTION.maximalNetworkRank(2, buildArray(1, 0)));
    System.out.println(SOLUTION.maximalNetworkRank(4, buildArray(0, 1, 0, 3, 1, 2, 1, 3)));
    System.out.println(SOLUTION.maximalNetworkRank(5, buildArray(0, 1, 0, 3, 1, 2, 1, 3, 2, 3, 2, 4)));
    System.out.println(SOLUTION.maximalNetworkRank(8, buildArray(0, 1, 1, 2, 2, 3, 2, 4, 5, 6, 5, 7)));
  }

  @Test
  public void test_restoreMatrix() {
    System.out.println(Arrays.deepToString(SOLUTION.restoreMatrix(new int[]{3, 8}, new int[]{4, 7})));
    System.out.println(Arrays.deepToString(SOLUTION.restoreMatrix(new int[]{5, 7, 10}, new int[]{8, 6, 8})));
    System.out.println(Arrays.deepToString(SOLUTION.restoreMatrix(new int[]{14, 9}, new int[]{6, 9, 8})));
    System.out.println(Arrays.deepToString(SOLUTION.restoreMatrix(new int[]{1, 0}, new int[]{1})));
    System.out.println(Arrays.deepToString(SOLUTION.restoreMatrix(new int[]{0}, new int[]{0})));
  }

  @Test
  public void test_minSubarray() {
    System.out.println(SOLUTION.minSubarray(new int[]{26, 19, 11, 14, 18, 4, 7, 1, 30, 23, 19, 8, 10, 6, 26, 3}, 26));
    System.out.println(SOLUTION.minSubarray(new int[]{3, 1, 4, 2}, 6));
    System.out.println(SOLUTION.minSubarray(new int[]{6, 3, 5, 2}, 9));
    System.out.println(SOLUTION.minSubarray(new int[]{1, 2, 3}, 3));
    System.out.println(SOLUTION.minSubarray(new int[]{1, 2, 3}, 7));
    System.out.println(SOLUTION.minSubarray(new int[]{1000000000, 1000000000, 1000000000}, 3));
  }

  @Test
  public void test_maxValue() {
    System.out.println(SOLUTION.maxValue(new int[][]{
      new int[]{1, 3, 1},
      new int[]{1, 5, 1},
      new int[]{4, 2, 1},
    }));
  }

  @Test
  public void test_getFolderNames() {
    System.out.println(Arrays.toString(SOLUTION.getFolderNames(new String[]{"kaido", "kaido(1)", "kaido", "kaido(1)", "kaido(2)"})));
    System.out.println(Arrays.toString(SOLUTION.getFolderNames(new String[]{"wano", "wano", "wano", "wano"})));
    System.out.println(Arrays.toString(SOLUTION.getFolderNames(new String[]{"pes", "fifa", "gta", "pes(2019)"})));
    System.out.println(Arrays.toString(SOLUTION.getFolderNames(new String[]{"gta", "gta(1)", "gta", "avalon"})));
    System.out.println(Arrays.toString(SOLUTION.getFolderNames(new String[]{"onepiece", "onepiece(1)", "onepiece(2)", "onepiece(3)", "onepiece"})));
    System.out.println(Arrays.toString(SOLUTION.getFolderNames(new String[]{"wano", "wano(1)", "wano(2)", "wano(3)"})));
    System.out.println(Arrays.toString(SOLUTION.getFolderNames(new String[]{"kaido", "kaido(1)", "kaido", "kaido(1)"})));
  }

  @Test
  public void test_circularPermutation() {
    SOLUTION.circularPermutation(4, 0).forEach(n -> System.out.println(b(n, 4)));
  }

  @Test
  public void test_grayCode() {
    for (int i = 1; i <= 5; i++) {
      System.out.println("===================== n=" + i + " =====================");
      int finalI = i;
      SOLUTION.grayCode(i).forEach(n -> System.out.println(b(n, finalI)));
    }
  }

  @Test
  public void test_maxAverageRatio() {
    System.out.println(SOLUTION.maxAverageRatio(buildArray(1, 2, 3, 5, 2, 2), 2));
    System.out.println(SOLUTION.maxAverageRatio(buildArray(2, 4, 3, 9, 4, 5, 2, 10), 4));
  }

  @Test
  public void test_findSolution() {
    CustomFunction function1 = Integer::sum;
    CustomFunction function2 = (x, y) -> x * y;
    System.out.println(SOLUTION.findSolution(function1, 5));
    System.out.println(SOLUTION.findSolution(function2, 5));
  }

  @Test
  public void test_longestWPI() {
    System.out.println(SOLUTION.longestWPI(new int[]{9, 9, 6, 0, 6, 6, 9}));
    System.out.println(SOLUTION.longestWPI(new int[]{6, 6, 6}));
  }

  @Test
  public void test_balancedString() {
    // WWEQERQWQWWRWWERQWEQ
    // System.out.println(((int) 'E'));
    // System.out.println(((int) 'Q'));
    // System.out.println(((int) 'R'));
    // System.out.println(((int) 'W'));
    // System.out.println("=====================================");
    // System.out.println('E' - 'E');
    // System.out.println('Q' - 'E');
    // System.out.println('R' - 'E');
    // System.out.println('W' - 'E');
    System.out.println(SOLUTION.balancedString("WWEQERQWQWWRWWERQWEQ"));
    // System.out.println(SOLUTION.balancedString("QWER"));
    // System.out.println(SOLUTION.balancedString("QQWE"));
    // System.out.println(SOLUTION.balancedString("QQQW"));
    // System.out.println(SOLUTION.balancedString("QQQQ"));
  }

  @Test
  public void test_shortestAlternatingPaths() {
    int[] a1 = SOLUTION.shortestAlternatingPaths(3, buildArray(0, 1, 1, 2), new int[0][0]);
    // int[] a2 = SOLUTION.shortestAlternatingPaths(3, buildArray(0, 1), buildArray(2, 1));
    // int[] a3 = SOLUTION.shortestAlternatingPaths(3, buildArray(1, 0), buildArray(2, 1));
    // int[] a4 = SOLUTION.shortestAlternatingPaths(3, buildArray(0, 1), buildArray(1, 2));
    // int[] a5 = SOLUTION.shortestAlternatingPaths(3, buildArray(0, 1, 0, 2), buildArray(1, 0));
  }

  @Test
  public void testAuthenticationManager() {
    AuthenticationManager obj = new AuthenticationManager(5);
    obj.renew("aaa", 1);
    obj.generate("aaa", 2);
    System.out.println(obj.countUnexpiredTokens(6));
    obj.generate("bbb", 7);
    obj.renew("aaa", 8);
    obj.renew("bbb", 10);
    System.out.println(obj.countUnexpiredTokens(15));
  }

  @Test
  public void test_waysToMakeFair() {
    System.out.println(SOLUTION.waysToMakeFair(new int[]{2, 1, 6, 4}));
    System.out.println(SOLUTION.waysToMakeFair(new int[]{1, 1, 1}));
    System.out.println(SOLUTION.waysToMakeFair(new int[]{1, 2, 3}));
  }

  @Test
  public void test_areSentencesSimilar() {
    System.out.println(SOLUTION.areSentencesSimilar("My name is Haley", "My Haley"));
    System.out.println(SOLUTION.areSentencesSimilar("of", "A lot of words"));
    System.out.println(SOLUTION.areSentencesSimilar("Eating right now", "Eating"));
    System.out.println(SOLUTION.areSentencesSimilar("Luky", "Luccckyy"));
    System.out.println(SOLUTION.areSentencesSimilar("d T d ED uXW L U J n klIe", "d T d ED uXW L U J klIe"));
    System.out.println(SOLUTION.areSentencesSimilar("A", "a A b A"));
  }

  @Test
  public void test_getKthMagicNumber() {
    // for (int i = 1; i < 100; i++) {
    //   if (isMagicNumber(i)) {
    //     System.out.println(i);
    //   }
    // }
    System.out.println(SOLUTION.getKthMagicNumber(2));
  }

  @Test
  public void test_kClosest() {

    int[][] actual1 = SOLUTION.kClosest(new int[][]{new int[]{1, 3}, new int[]{-2, 2}}, 50);
    Assert.assertArrayEquals(new int[][]{new int[]{1, 3}, new int[]{-2, 2}}, actual1);

    int[][] actual2 = SOLUTION.kClosest(new int[][]{new int[]{1, 3}, new int[]{-2, 2}, new int[]{2, -2}}, 2);
    Assert.assertArrayEquals(new int[][]{new int[]{-2, 2}, new int[]{2, -2}}, actual2);
  }

  @Test
  public void test_removeSubfolders() {
    System.out.println(SOLUTION.removeSubfolders(new String[]{"/a", "/a/b/c", "/a/b/d"}));
    System.out.println(SOLUTION.removeSubfolders(new String[]{"/a/b/c", "/a/b/ca", "/a/b/d"}));
  }

  @Test
  public void test_combinationSum() {
    System.out.println(SOLUTION.combinationSum(new int[]{2, 3, 6, 7}, 7));
    System.out.println(SOLUTION.combinationSum(new int[]{2, 3, 5}, 8));
  }
}
