package alice.exercise.leetcode.middle;

/**
 * @author Alice
 * @since 2023-02-20 星期一 9:49
 */
public interface CustomFunction {
  /**
   * Returns some positive integer f(x, y) for two positive integers x and y based on a formula.
   *
   * @param x x
   * @param y y
   * @return function
   */
  int f(int x, int y);
}
