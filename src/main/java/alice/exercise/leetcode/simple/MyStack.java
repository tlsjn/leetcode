package alice.exercise.leetcode.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * 225. 用队列实现栈
 *
 * @author Alice
 * @since 2023-02-14 星期二 16:00
 */
class MyStack {
  private final List<Integer> elements;
  private int size;

  public MyStack() {
    elements = new ArrayList<>();
    size = 0;
  }

  public void push(int x) {
    elements.add(size++, x);
  }

  public int pop() {
    return elements.get(--size);
  }

  public int top() {
    return elements.get(size - 1);
  }

  public boolean empty() {
    return size == 0;
  }
}
