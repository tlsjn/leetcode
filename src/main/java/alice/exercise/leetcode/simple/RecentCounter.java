package alice.exercise.leetcode.simple;

/**
 * 933. 最近的请求次数
 *
 * @author Alice
 * @since 2023-03-22 星期三 14:36
 */
public class RecentCounter {

  private static final int THREAD_HOLD = 3000;
  private final int[] DATA;
  private int left;
  private int right;

  public RecentCounter() {
    DATA = new int[10005];
    left = 0;
    right = 0;
  }

  public int ping(int t) {
    DATA[right++] = t;
    while (t - DATA[left] > THREAD_HOLD) {
      left++;
    }
    return right - left;
  }
}
