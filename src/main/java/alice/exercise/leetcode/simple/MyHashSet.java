package alice.exercise.leetcode.simple;

import java.util.LinkedList;

/**
 * 705. 设计哈希集合
 *
 * @author Alice
 * @since 2023-02-21 星期二 18:11
 */
public class MyHashSet {
  private static final int BASE = 997;
  private final LinkedList<Integer>[] values;

  public MyHashSet() {
    values = new LinkedList[BASE];
  }

  public void add(int key) {
    int location = key % BASE;
    if (values[location] == null) {
      values[location] = new LinkedList<Integer>();
      values[location].add(key);
      return;
    }
    for (Integer n : values[location]) {
      if (n == key) {
        return;
      }
    }
    values[location].addLast(key);
  }

  public void remove(int key) {
    int location = key % BASE;
    if (values[location] == null) {
      return;
    }
    for (Integer candidate : values[location]) {
      if (candidate == key) {
        values[location].remove(candidate);
        return;
      }
    }
  }

  public boolean contains(int key) {
    int location = key % BASE;
    if (values[location] == null) {
      return false;
    }
    for (Integer candidate : values[location]) {
      if (candidate == key) {
        return true;
      }
    }
    return false;
  }

}
