package alice.exercise.leetcode.simple;

import alice.exercise.leetcode.entity.ListNode;
import alice.exercise.leetcode.entity.TreeNode;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import static alice.exercise.leetcode.util.SolutionUtil.*;

/**
 * @author Alice
 * @since 2023-02-06 星期一 9:29
 */
public class MainTest {
  private static final Solution SOLUTION = new Solution();
  private static final TreeNode TREE_NODE;

  static {
    TreeNode t4 = new TreeNode(4);
    TreeNode t5 = new TreeNode(5);
    TreeNode t6 = new TreeNode(6);
    TreeNode t7 = new TreeNode(7);
    TreeNode left = new TreeNode(2, t7, new TreeNode(9, null, t4));
    TreeNode right = new TreeNode(3, t6, new TreeNode(8, t5, null));
    TREE_NODE = new TreeNode(1, left, right);
  }

  @Test
  public void testRemoveOuterParentheses() {
    System.out.println(SOLUTION.removeOuterParentheses("(()())(())"));
    System.out.println(SOLUTION.removeOuterParentheses("(()())(())(()(()))"));
    System.out.println(SOLUTION.removeOuterParentheses("()()"));
  }

  @Test
  public void test_circularGameLosers() {
    Map<Integer, Character> map = new HashMap<>();
    for (int i = 40; i <= 130; i++) {
      map.put(i, (char)i);
    }
    System.out.println(Arrays.toString(SOLUTION.circularGameLosers(2, 1)));
    System.out.println(Arrays.toString(SOLUTION.circularGameLosers(5, 2)));
    System.out.println(Arrays.toString(SOLUTION.circularGameLosers(4, 4)));
  }

  @Test
  public void test_minLength() {
    System.out.println(SOLUTION.minLength("ABFCACDB"));
    System.out.println(SOLUTION.minLength("ACBBD"));
  }

  @Test
  public void test_reversePrefix() {
    System.out.println(SOLUTION.minOperations("10010100") + ": " + SOLUTION.minOperations1("10010100"));
    System.out.println(SOLUTION.minOperations("0100") + ": " + SOLUTION.minOperations1("0100"));
    System.out.println(SOLUTION.minOperations("10") + ": " + SOLUTION.minOperations1("10"));
    System.out.println(SOLUTION.minOperations("1111") + ": " + SOLUTION.minOperations1("1111"));
  }

  @Test
  public void test_countBalls() {
    System.out.println(SOLUTION.countBalls(1, 10));
    System.out.println(SOLUTION.countBalls(5, 15));
    System.out.println(SOLUTION.countBalls(19, 28));
  }

  @Test
  public void test_decrypt() {
    System.out.println(Arrays.toString(SOLUTION.decrypt(new int[]{5, 7, 1, 4}, 3)));
    System.out.println(Arrays.toString(SOLUTION.decrypt(new int[]{1, 2, 3, 4}, 0)));
    System.out.println(Arrays.toString(SOLUTION.decrypt(new int[]{2, 4, 9, 3}, -2)));
  }

  @Test
  public void test_sumRootToLeaf() {
    System.out.println(SOLUTION.sumRootToLeaf(new TreeNode(1, new TreeNode(0, new TreeNode(0), new TreeNode(1)), new TreeNode(1, new TreeNode(0), new TreeNode(1)))));
  }

  @Test
  public void test_arrayRankTransform() {
    System.out.println(Arrays.toString(SOLUTION.arrayRankTransform(new int[]{40, 10, 20, 30})));
    System.out.println(Arrays.toString(SOLUTION.arrayRankTransform(new int[]{100, 100, 100})));
    System.out.println(Arrays.toString(SOLUTION.arrayRankTransform(new int[]{37, 12, 28, 9, 100, 56, 80, 5, 12})));
  }

  @Test
  public void test_areNumbersAscending() {
    System.out.println(SOLUTION.timeRequiredToBuy(new int[]{2, 3, 2}, 2));
    System.out.println(SOLUTION.timeRequiredToBuy(new int[]{5, 1, 1, 1}, 0));
  }

  @Test
  public void test_countOperations() {
    System.out.println(Arrays.toString(SOLUTION.sortEvenOdd(new int[]{0, 1, 2, 3, 4, 5, 6, 7})));
    System.out.println(Arrays.toString(SOLUTION.sortEvenOdd(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8})));
    System.out.println(Arrays.toString(SOLUTION.sortEvenOdd(new int[]{4, 1, 2, 3})));
    System.out.println(Arrays.toString(SOLUTION.sortEvenOdd(new int[]{2, 1})));
  }

  @Test
  public void test_equalFrequency() {
    System.out.println(SOLUTION.equalFrequency("acbda"));
    System.out.println(SOLUTION.equalFrequency("aca"));
    System.out.println(SOLUTION.equalFrequency("abbcc"));
    System.out.println(SOLUTION.equalFrequency("abcc"));
    System.out.println(SOLUTION.equalFrequency("cbccca"));
    System.out.println(SOLUTION.equalFrequency("aazz"));
    System.out.println(SOLUTION.equalFrequency("bac"));
  }

  @Test
  public void test_getCommon() {
    System.out.println(SOLUTION.getCommon(new int[]{1, 2, 3}, new int[]{2, 4}));
    System.out.println(SOLUTION.getCommon(new int[]{1, 2, 3, 6}, new int[]{2, 3, 4, 5}));
  }

  @Test
  public void test_findKthPositive() {
    System.out.println(SOLUTION.findKthPositive(new int[]{2, 3, 4, 7, 11}, 5));
    System.out.println(SOLUTION.findKthPositive(new int[]{1, 2, 3, 4}, 2));
  }

  @Test
  public void test_countVowelSubstrings() {
    System.out.println(SOLUTION.countVowelSubstrings("duuebuaeeeeeeuaoeiueaoui"));
    System.out.println(SOLUTION.countVowelSubstrings("aeiouu"));
    System.out.println(SOLUTION.countVowelSubstrings("cuaieuouac"));
  }

  @Test
  public void test_findDifference() {
    System.out.println(SOLUTION.findDifference(new int[]{1, 2, 3, 3}, new int[]{1, 1, 2, 2}));
  }

  @Test
  public void test_findKDistantIndices() {
    System.out.println(SOLUTION.findKDistantIndices(new int[]{2, 2, 2, 2, 2}, 2, 2));
    System.out.println(SOLUTION.findKDistantIndices(new int[]{
      222, 151, 842, 244, 103, 736, 219, 432, 565, 216, 36, 198, 10, 367, 778, 111, 307, 460, 92, 622, 750, 36, 559, 983, 782, 432, 312, 111, 676, 179, 44, 86, 766, 371, 746, 905, 850, 170, 892, 80, 449, 932, 295, 875, 259, 556, 730, 441, 153, 869
    }, 153, 19));
  }

  @Test
  public void test_maximumPopulation() {
    System.out.println(SOLUTION.maximumPopulation(buildArray(2008, 2026, 2004, 2008, 2034, 2035, 1999, 2050, 2049, 2050, 2011, 2035, 1966, 2033, 2044, 2049)));
  }

  @Test
  public void test_specialArray() {
    System.out.println(SOLUTION.specialArray(new int[]{3, 5}));
    System.out.println(SOLUTION.specialArray(new int[]{0, 0}));
    System.out.println(SOLUTION.specialArray(new int[]{0, 4, 3, 0, 4}));
    System.out.println(SOLUTION.specialArray(new int[]{3, 6, 7, 7, 0}));
  }

  @Test
  public void test_countElements() {
    System.out.println(SOLUTION.countElements(new int[]{-3, 3, 3, 90}));
  }

  @Test
  public void test_mostFrequentEven() {
    System.out.println(SOLUTION.mostFrequentEven(new int[]{8154, 9139, 8194, 3346, 5450, 9190, 133, 8239, 4606, 8671, 8412, 6290}));
  }

  @Test
  public void test_capitalizeTitle() {
    System.out.println(SOLUTION.capitalizeTitle("capiTalIze tHe titLe"));
    // 大转小 x | 0x20
    // 小转大 (1 << 6) | (x & 1f)
  }

  @Test
  public void test_numWaterBottles() {
    System.out.println(SOLUTION.numWaterBottles(9, 3));
  }

  @Test
  public void test_subtractProductAndSum() {
    System.out.println(SOLUTION.oddCells(48, 37, buildArray(40, 5)));
    System.out.println(SOLUTION.oddCells(2, 3, buildArray(0, 1, 1, 1)));
    System.out.println(SOLUTION.oddCells(2, 2, buildArray(1, 1, 0, 0)));
  }

  @Test
  public void test_maximum69Number() {
    System.out.println(SOLUTION.maximum69Number(9669));
  }

  @Test
  public void test_sortByBits() {
    System.out.println(Arrays.toString(SOLUTION.sortByBits(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8})));
    System.out.println(Arrays.toString(SOLUTION.sortByBits(new int[]{1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1})));
    System.out.println(Arrays.toString(SOLUTION.sortByBits(new int[]{10000, 10000})));
    System.out.println(Arrays.toString(SOLUTION.sortByBits(new int[]{2, 3, 5, 7, 11, 13, 17, 19})));
    System.out.println(Arrays.toString(SOLUTION.sortByBits(new int[]{10, 100, 1000, 10000})));
  }

  @Test
  public void test_luckyNumbers() {
    System.out.println(SOLUTION.luckyNumbers(new int[][]{
      new int[]{1, 10, 4, 2}, new int[]{9, 3, 8, 7}, new int[]{15, 16, 17, 12}
    }));
  }

  @Test
  public void test_reformat() {
    System.out.println(SOLUTION.reformat("a0b1c2"));
    System.out.println(SOLUTION.reformat("leetcode"));
    System.out.println(SOLUTION.reformat("1229857369"));
    System.out.println(SOLUTION.reformat("covid2019"));
    System.out.println(SOLUTION.reformat("ab123"));
  }

  @Test
  public void test_dayOfTheWeek() {
    System.out.println(SOLUTION.dayOfTheWeek(31, 8, 2019));
    System.out.println(SOLUTION.dayOfTheWeek(18, 7, 1999));
    System.out.println(SOLUTION.dayOfTheWeek(15, 8, 1993));
  }

  @Test
  public void test_minCostToMoveChips() {
    System.out.println(SOLUTION.minCostToMoveChips(new int[]{1, 2, 3}));
    System.out.println(SOLUTION.minCostToMoveChips(new int[]{2, 2, 2, 3, 3}));
    System.out.println(SOLUTION.minCostToMoveChips(new int[]{1, 1000000000}));
  }

  @Test
  public void test_minimumAbsDifference() {
    System.out.println(SOLUTION.minimumAbsDifference(new int[]{4, 2, 1, 3}));
    System.out.println(SOLUTION.minimumAbsDifference(new int[]{1, 3, 6, 10, 15}));
    System.out.println(SOLUTION.minimumAbsDifference(new int[]{3, 8, -10, 23, 19, -4, -14, 27}));
  }

  @Test
  public void test_maxNumberOfBalloons() {
    System.out.println(SOLUTION.maxNumberOfBalloons("nlaebolko"));
    System.out.println(SOLUTION.maxNumberOfBalloons("loonbalxballpoon"));
    System.out.println(SOLUTION.maxNumberOfBalloons("leetcode"));
  }

  @Test
  public void test_uniqueOccurrences() {
    System.out.println(SOLUTION.uniqueOccurrences(new int[]{1, 2, 2, 1, 1, 3}));
    System.out.println(SOLUTION.uniqueOccurrences(new int[]{1, 2}));
    System.out.println(SOLUTION.uniqueOccurrences(new int[]{-3, 0, 1, -3, 1, 1, 1, -3, 10, 0}));
  }

  @Test
  public void test_checkStraightLine() {
    System.out.println(SOLUTION.checkStraightLine(buildArray(0, 0, 0, 1, 0, -1)));
    System.out.println(SOLUTION.checkStraightLine(buildArray(1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7)));
    System.out.println(SOLUTION.checkStraightLine(buildArray(1, 1, 2, 2, 3, 4, 4, 5, 5, 6, 7, 7)));
  }

  @Test
  public void test_checkDistances() {
    System.out.println(SOLUTION.checkDistances("abcabc", new int[]{0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
    System.out.println(SOLUTION.checkDistances("abaccb", new int[]{1, 3, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
    System.out.println(SOLUTION.checkDistances("aa", new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
  }

  @Test
  public void test_countLargestGroup() {
    System.out.println(SOLUTION.countLargestGroup(13));
    System.out.println(SOLUTION.countLargestGroup(2));
    System.out.println(SOLUTION.countLargestGroup(15));
    System.out.println(SOLUTION.countLargestGroup(24));
  }

  @Test
  public void test_findLucky() {
    System.out.println(SOLUTION.findLucky(new int[]{2, 2, 2, 3, 3}));
  }

  @Test
  public void test_distanceBetweenBusStops() {
    System.out.println(SOLUTION.distanceBetweenBusStops(new int[]{1, 2, 3, 4}, 0, 1));
    System.out.println(SOLUTION.distanceBetweenBusStops(new int[]{1, 2, 3, 4}, 0, 2));
    System.out.println(SOLUTION.distanceBetweenBusStops(new int[]{1, 2, 3, 4}, 0, 3));
  }

  @Test
  public void test_diStringMatch() {
    System.out.println(Arrays.toString(SOLUTION.diStringMatch("IDID")));
    System.out.println(Arrays.toString(SOLUTION.diStringMatch("III")));
    System.out.println(Arrays.toString(SOLUTION.diStringMatch("DDI")));
  }

  @Test
  public void test_numPrimeArrangements() {
    System.out.println(SOLUTION.numPrimeArrangements(5));
    System.out.println(SOLUTION.numPrimeArrangements(100));
  }

  @Test
  public void test_minDeletionSize() {
    System.out.println(SOLUTION.minDeletionSize(new String[]{"cba", "daf", "ghi"}));
    System.out.println(SOLUTION.minDeletionSize(new String[]{"a", "b"}));
    System.out.println(SOLUTION.minDeletionSize(new String[]{"zyx", "wvu", "tsr"}));
  }

  @Test
  public void test_repeatedNTimes() {
    System.out.println(SOLUTION.repeatedNTimes(new int[]{1, 2, 3, 3}));
    System.out.println(SOLUTION.repeatedNTimes(new int[]{2, 1, 2, 5, 3, 2}));
    System.out.println(SOLUTION.repeatedNTimes(new int[]{5, 1, 5, 2, 5, 3, 5, 4}));
  }

  @Test
  public void test_unequalTriplets() {
    System.out.println(SOLUTION.unequalTriplets(new int[]{4, 4, 2, 4, 3}));
    System.out.println(SOLUTION.unequalTriplets(new int[]{1, 1, 1, 1, 1}));
  }

  @Test
  public void test_oddString() {
    System.out.println(SOLUTION.oddString(new String[]{"ddd", "poo", "baa", "onn"}));
    System.out.println(SOLUTION.oddString(new String[]{"mll", "edd", "jii", "tss", "fee", "dcc", "nmm", "abb", "utt", "zyy", "xww", "tss", "wvv", "xww", "utt"}));
    System.out.println(SOLUTION.oddString(new String[]{"adc", "wzy", "abc"}));
    System.out.println(SOLUTION.oddString(new String[]{"aaa", "bob", "ccc", "ddd"}));
  }

  @Test
  public void test_differenceOfSum() {
    System.out.println(SOLUTION.differenceOfSum(new int[]{1, 15, 6, 3}));
    System.out.println(SOLUTION.differenceOfSum(new int[]{1, 2, 3, 4}));
  }

  @Test
  public void test_arithmeticTriplets() {
    System.out.println(SOLUTION.arithmeticTriplets(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200}, 1));
    System.out.println(SOLUTION.arithmeticTriplets(new int[]{0, 1, 4, 6, 7, 10}, 3));
    System.out.println(SOLUTION.arithmeticTriplets(new int[]{4, 5, 6, 7, 8, 9}, 2));
  }

  @Test
  public void test_findTheArrayConcVal() {
    System.out.println(SOLUTION.findTheArrayConcVal(new int[]{7, 52, 2, 4}));
    System.out.println(SOLUTION.findTheArrayConcVal(new int[]{5, 14, 13, 8, 12}));
  }

  @Test
  public void test_leftRigthDifference() {
    System.out.println(Arrays.toString(SOLUTION.leftRigthDifference(new int[]{10, 4, 8, 3})));
    System.out.println(Arrays.toString(SOLUTION.leftRigthDifference(new int[]{1})));
  }

  @Test
  public void test_intersection1() {
    System.out.println(SOLUTION.intersection(new int[][]{
      new int[]{3, 1, 2, 4, 5},
      new int[]{1, 2, 3, 4},
      new int[]{3, 4, 5, 6}
    }));
    System.out.println(SOLUTION.intersection(new int[][]{
      new int[]{1, 2, 3},
      new int[]{4, 5, 6}
    }));
  }

  @Test
  public void test_distMoney() {
    System.out.println(SOLUTION.distMoney(2, 2));
    System.out.println(SOLUTION.distMoney(20, 3));
    System.out.println(SOLUTION.distMoney(16, 2));
  }

  @Test
  public void test_evenOddBit() {
    System.out.println(Arrays.toString(SOLUTION.evenOddBit(17)));
    System.out.println(Arrays.toString(SOLUTION.evenOddBit(2)));
  }

  @Test
  public void test_tribonacci() {
    StringJoiner joiner = new StringJoiner(",");
    for (int i = 0; i <= 37; i++) {
      System.out.println("i = " + i);
      joiner.add("" + SOLUTION.tribonacci(i));
    }
    System.out.println(joiner);
  }

  @Test
  public void test_numEquivDominoPairs() {
    System.out.println(SOLUTION.numEquivDominoPairs(buildArray(1, 2, 2, 1, 3, 4, 5, 6)));
  }

  @Test
  public void test_countCharacters() {
    System.out.println(SOLUTION.countCharacters(new String[]{"cat", "bt", "hat", "tree"}, "atach"));
    System.out.println(SOLUTION.countCharacters(new String[]{"hello", "world", "leetcode"}, "welldonehoneyr"));
  }

  @Test
  public void test_relativeSortArray() {
    System.out.println(Arrays.toString(SOLUTION.relativeSortArray(new int[]{943, 790, 427, 722, 860, 550, 225, 846, 715, 320}, new int[]{943, 715, 427, 790, 860, 722, 225, 320, 846, 550})));
    System.out.println(Arrays.toString(SOLUTION.relativeSortArray(new int[]{26, 21, 11, 20, 50, 34, 1, 18}, new int[]{21, 11, 26, 20})));
    System.out.println(Arrays.toString(SOLUTION.relativeSortArray(new int[]{2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19}, new int[]{2, 1, 4, 3, 9, 6})));
    System.out.println(Arrays.toString(SOLUTION.relativeSortArray(new int[]{28, 6, 22, 8, 44, 17}, new int[]{22, 28, 8, 6})));
  }

  @Test
  public void test_dayOfYear() {
    System.out.println(SOLUTION.dayOfYear("2019-02-28"));
    System.out.println(SOLUTION.dayOfYear("2019-02-29"));
    System.out.println(SOLUTION.dayOfYear("2019-01-09"));
    System.out.println(SOLUTION.dayOfYear("2019-02-10"));
  }

  @Test
  public void test_smallerNumbersThanCurrent() {
    // 4,0,1,1,3
    System.out.println(Arrays.toString(SOLUTION.smallerNumbersThanCurrent(new int[]{8, 1, 2, 2, 3})));
    // 2,1,0,3
    System.out.println(Arrays.toString(SOLUTION.smallerNumbersThanCurrent(new int[]{6, 5, 4, 8})));
    System.out.println(Arrays.toString(SOLUTION.smallerNumbersThanCurrent(new int[]{7, 7, 7, 7})));
  }

  @Test
  public void test_gcdOfStrings() {
    System.out.println(SOLUTION.gcdOfStrings("ABCABC", "ABC"));
    System.out.println(SOLUTION.gcdOfStrings("ABABAB", "ABAB"));
    System.out.println(SOLUTION.gcdOfStrings("LEET", "CODE"));
  }

  @Test
  public void test_isBoomerang() {
    System.out.println(SOLUTION.isBoomerang(buildArray(1, 0, 1, 2, 0, 1)));
  }

  @Test
  public void test_canThreePartsEqualSum() {
    System.out.println(SOLUTION.canThreePartsEqualSum(new int[]{0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1}));
    System.out.println(SOLUTION.canThreePartsEqualSum(new int[]{0, 2, 1, -6, 6, 7, 9, -1, 2, 0, 1}));
    System.out.println(SOLUTION.canThreePartsEqualSum(new int[]{3, 3, 6, 5, -2, 2, 5, 1, -9, 4}));
  }

  @Test
  public void test_hasAlternatingBits() {
    System.out.println(SOLUTION.hasAlternatingBits(4));
    System.out.println(SOLUTION.hasAlternatingBits(5));
    System.out.println(SOLUTION.hasAlternatingBits(7));
    System.out.println(SOLUTION.hasAlternatingBits(11));
  }

  @Test
  public void test_removeDuplicates() {
    System.out.println(SOLUTION.removeDuplicates("abbaca"));
    // "ibfjcdidiaidchakchchcahabhibdcejkdkfbecdjhajbkfebebfea"
    System.out.println(SOLUTION.removeDuplicates("ibfjcaffccadidiaidchakchchcahabhibdcejkdkfbaeeaecdjhajbkfebebfea"));
  }

  @Test
  public void test_validMountainArray() {
    System.out.println(SOLUTION.validMountainArray(new int[]{2, 1}));
    System.out.println(SOLUTION.validMountainArray(new int[]{3, 5, 5}));
    System.out.println(SOLUTION.validMountainArray(new int[]{0, 3, 2, 1}));
    System.out.println(SOLUTION.validMountainArray(new int[]{4, 3, 2, 1}));
    System.out.println(SOLUTION.validMountainArray(new int[]{1, 2, 3, 4}));
  }

  @Test
  public void test_heightChecker() {
    System.out.println(SOLUTION.heightChecker(new int[]{2, 1, 2, 1, 1, 2, 2, 1}));
    System.out.println(SOLUTION.heightChecker(new int[]{1, 1, 4, 2, 1, 3}));
    System.out.println(SOLUTION.heightChecker(new int[]{5, 1, 2, 3, 4}));
    System.out.println(SOLUTION.heightChecker(new int[]{1, 2, 3, 4, 5}));
  }

  @Test
  public void test_isAlienSorted() {
    System.out.println(SOLUTION.isAlienSorted(new String[]{"hello", "leetcode"}, "hlabcdefgijkmnopqrstuvwxyz"));
    System.out.println(SOLUTION.isAlienSorted(new String[]{"word", "world", "row"}, "worldabcefghijkmnpqstuvxyz"));
    System.out.println(SOLUTION.isAlienSorted(new String[]{"apple", "app"}, "abcdefghijklmnopqrstuvwxyz"));
  }

  @Test
  public void test_commonChars() {
    System.out.println(SOLUTION.commonChars(new String[]{"bella", "label", "roller"}));
    System.out.println(SOLUTION.commonChars(new String[]{"cool", "lock", "cook"}));
  }

  @Test
  public void test_addToArrayForm() {
    System.out.println(SOLUTION.addToArrayForm(new int[]{1, 2, 0, 0}, 34));
    System.out.println(SOLUTION.addToArrayForm(new int[]{2, 7, 4}, 181));
    System.out.println(SOLUTION.addToArrayForm(new int[]{2, 1, 5}, 806));
  }

  @Test
  public void test_isRectangleOverlap() {
    System.out.println(SOLUTION.isRectangleOverlap(new int[]{0, 0, 1, 1}, new int[]{1, 0, 2, 1}));
    System.out.println(SOLUTION.isRectangleOverlap(new int[]{5, 15, 8, 18}, new int[]{0, 3, 7, 9}));
    System.out.println(SOLUTION.isRectangleOverlap(new int[]{0, 0, 1, 1}, new int[]{2, 2, 3, 3}));
    System.out.println(SOLUTION.isRectangleOverlap(new int[]{0, 0, 2, 2}, new int[]{1, 1, 3, 3}));
    System.out.println(SOLUTION.isRectangleOverlap(new int[]{7, 8, 13, 15}, new int[]{10, 8, 12, 20}));
  }

  @Test
  public void test_hasGroupsSizeX() {
    System.out.println(SOLUTION.hasGroupsSizeX(new int[]{1, 1, 1, 1, 2, 2, 2, 2, 2, 2}));
    System.out.println(SOLUTION.hasGroupsSizeX(new int[]{1, 1, 2, 2, 2, 2}));
    System.out.println(SOLUTION.hasGroupsSizeX(new int[]{1, 2, 3, 4, 4, 3, 2, 1}));
    System.out.println(SOLUTION.hasGroupsSizeX(new int[]{1, 1, 1, 2, 2, 2, 3, 3}));
  }

  @Test
  public void testRecentCounter() {
    RecentCounter counter = new RecentCounter();
    System.out.println(counter.ping(1)); // 1
    System.out.println(counter.ping(100)); // 2
    System.out.println(counter.ping(3001)); // 3
    System.out.println(counter.ping(3002)); // 3
  }

  @Test
  public void test_surfaceArea() {
    // 34
    System.out.println(SOLUTION.surfaceArea(new int[][]{
      new int[]{1, 2},
      new int[]{3, 4}
    }));
    // 32
    System.out.println(SOLUTION.surfaceArea(new int[][]{
      new int[]{1, 1, 1},
      new int[]{1, 0, 1},
      new int[]{1, 1, 1},
    }));
    // 46
    System.out.println(SOLUTION.surfaceArea(new int[][]{
      new int[]{2, 2, 2},
      new int[]{2, 1, 2},
      new int[]{2, 2, 2},
    }));
  }

  @Test
  public void test_reverseOnlyLetters() {
    System.out.println(SOLUTION.reverseOnlyLetters("ab-cd"));
    System.out.println(SOLUTION.reverseOnlyLetters("a-bC-dEf-ghIj"));
    System.out.println(SOLUTION.reverseOnlyLetters("Test1ng-Leet=code-Q!"));
  }

  @Test
  public void test_sortArrayByParityII() {
    System.out.println(Arrays.toString(SOLUTION.sortArrayByParityII(new int[]{4, 2, 5, 7})));
    System.out.println(Arrays.toString(SOLUTION.sortArrayByParityII(new int[]{2, 3})));
  }

  @Test
  public void test_numUniqueEmails() {
    System.out.println(SOLUTION.numUniqueEmails(new String[]{"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"}));
    System.out.println(SOLUTION.numUniqueEmails(new String[]{"a@leetcode.com", "b@leetcode.com", "c@leetcode.com"}));
  }

  @Test
  public void test_findLengthOfLCIS() {
    System.out.println(SOLUTION.findLengthOfLCIS(new int[]{1, 3, 5, 7}));
    System.out.println(SOLUTION.findLengthOfLCIS(new int[]{1, 3, 5, 4, 7}));
    System.out.println(SOLUTION.findLengthOfLCIS(new int[]{2, 2, 2, 2, 2}));
  }

  @Test
  public void test_imageSmoother() {
    System.out.println(Arrays.deepToString(SOLUTION.imageSmoother(new int[][]{
      new int[]{1, 1, 1},
      new int[]{1, 0, 1},
      new int[]{1, 1, 1}
    })));
    System.out.println(Arrays.deepToString(SOLUTION.imageSmoother(new int[][]{
      new int[]{100, 200, 100},
      new int[]{200, 50, 200},
      new int[]{100, 200, 100}
    })));
  }

  @Test
  public void test_judgeCircle() {
    System.out.println(SOLUTION.judgeCircle("RLUURDDDLU"));
    System.out.println(SOLUTION.judgeCircle("UD"));
    System.out.println(SOLUTION.judgeCircle("LL"));
  }

  @Test
  public void test_findMaxAverage() {
    System.out.println(SOLUTION.findMaxAverage(new int[]{4, 2, 1, 3, 3}, 2));
    System.out.println(SOLUTION.findMaxAverage(new int[]{0, 4, 0, 3, 2}, 1));
    System.out.println(SOLUTION.findMaxAverage(new int[]{1, 12, -5, -6, 50, 3}, 4));
    System.out.println(SOLUTION.findMaxAverage(new int[]{5}, 1));
  }

  @Test
  public void test_maximumProduct() {
    System.out.println(SOLUTION.maximumProduct(new int[]{1, 2, 3}));
    System.out.println(SOLUTION.maximumProduct(new int[]{1, 2, 3, 4}));
    System.out.println(SOLUTION.maximumProduct(new int[]{-1, -2, -3}));
  }

  @Test
  public void test_floodFill() {
    System.out.println(Arrays.deepToString(SOLUTION.floodFill(new int[][]{
      new int[]{1, 1, 1},
      new int[]{1, 1, 0},
      new int[]{1, 0, 1}
    }, 1, 1, 2)));
    System.out.println(Arrays.deepToString(SOLUTION.floodFill(new int[][]{
      new int[]{0, 0, 0},
      new int[]{0, 0, 0}
    }, 0, 0, 2)));
  }

  @Test
  public void test_projectionArea() {
    System.out.println(SOLUTION.projectionArea(new int[][]{
      new int[]{1, 2},
      new int[]{3, 4}
    }));
    System.out.println(SOLUTION.projectionArea(new int[][]{new int[]{2}}));
    System.out.println(SOLUTION.projectionArea(new int[][]{
      new int[]{1, 0},
      new int[]{0, 2}
    }));
  }

  @Test
  public void test_KthLargest() {
    KthLargest largest = new KthLargest(3, new int[]{4, 5, 8, 2});
    System.out.println(largest.add(3));
    System.out.println(largest.add(5));
    System.out.println(largest.add(10));
    System.out.println(largest.add(9));
    System.out.println(largest.add(4));
  }

  @Test
  public void test_flipAndInvertImage() {
    System.out.println(Arrays.deepToString(SOLUTION.flipAndInvertImage(new int[][]{
      new int[]{1, 1, 0},
      new int[]{1, 0, 1},
      new int[]{0, 0, 0}
    })));
    System.out.println(Arrays.deepToString(SOLUTION.flipAndInvertImage(new int[][]{
      new int[]{1, 1, 0, 0},
      new int[]{1, 0, 0, 1},
      new int[]{0, 1, 1, 1},
      new int[]{1, 0, 1, 0}
    })));
  }

  @Test
  public void test_lemonadeChange() {
    System.out.println(SOLUTION.lemonadeChange(new int[]{5, 5, 5, 10, 20}));
    System.out.println(SOLUTION.lemonadeChange(new int[]{5, 5, 10, 10, 20}));
  }

  @Test
  public void test_answerQueries() {
    System.out.println(Arrays.toString(SOLUTION.answerQueries(new int[]{624082}, new int[]{972985, 564269, 607119, 693641, 787608, 46517, 500857, 140097})));
    System.out.println(Arrays.toString(SOLUTION.answerQueries(new int[]{4, 5, 2, 1}, new int[]{3, 10, 21})));
    System.out.println(Arrays.toString(SOLUTION.answerQueries(new int[]{2, 3, 4, 5}, new int[]{1})));
  }

  @Test
  public void test_findLHS() {
    System.out.println(SOLUTION.findLHS(new int[]{0, 3, 1, 3, 3, 3, 0, 1, 0, 2, 0, 3, 1, 3, -3, 2, 0, 3, 1, 2, 2, -3, 2, 2, 3, 3}));
    System.out.println(SOLUTION.findLHS(new int[]{-3, -1, -1, -1, -3, -2}));
    System.out.println(SOLUTION.findLHS(new int[]{1, 3, 2, 2, 5, 2, 3, 7}));
    System.out.println(SOLUTION.findLHS(new int[]{1, 2, 3, 4}));
    System.out.println(SOLUTION.findLHS(new int[]{1, 1, 1, 1}));
  }

  @Test
  public void test_minNumberOfHours() {
    System.out.println(SOLUTION.minNumberOfHours(1, 1, new int[]{1, 1, 1, 1}, new int[]{1, 1, 1, 50}));
    System.out.println(SOLUTION.minNumberOfHours(5, 3, new int[]{1, 4, 3, 2}, new int[]{2, 6, 3, 1}));
    System.out.println(SOLUTION.minNumberOfHours(2, 4, new int[]{1}, new int[]{3}));
  }

  @Test
  public void test_minimumRecolors() {
    System.out.println(SOLUTION.minimumRecolors("WBBWWBBWBW", 7));
    System.out.println(SOLUTION.minimumRecolors("BWBBBWBBBBWBWWBWBBBBBWWBBWWBBWBWBBBBBWWBBBWB", 33));
    System.out.println(SOLUTION.minimumRecolors("BWWWBB", 6));
    System.out.println(SOLUTION.minimumRecolors("WBWBBBW", 2));
  }

  @Test
  public void test_distributeCandies() {
    System.out.println(Arrays.toString(SOLUTION.distributeCandies(1000000000, 1000)));
    System.out.println(Arrays.toString(SOLUTION.distributeCandies(7, 4)));
    System.out.println(Arrays.toString(SOLUTION.distributeCandies(10, 3)));
  }

  @Test
  public void test_isLongPressedName() {
    System.out.println(SOLUTION.isLongPressedName("alexd", "aleex"));
    System.out.println(SOLUTION.isLongPressedName("alexd", "alexx"));
    System.out.println(SOLUTION.isLongPressedName("alexd", "ale"));
    System.out.println(SOLUTION.isLongPressedName("vtkgn", "vttkgnn"));
    System.out.println(SOLUTION.isLongPressedName("alex", "aaleexza"));
    System.out.println(SOLUTION.isLongPressedName("rick", "kric"));
    System.out.println(SOLUTION.isLongPressedName("alex", "aaleex"));
    System.out.println(SOLUTION.isLongPressedName("saeed", "ssaaedd"));
  }

  @Test
  public void test_smallestRangeI() {
    System.out.println(SOLUTION.smallestRangeI(new int[]{1}, 0));
    System.out.println(SOLUTION.smallestRangeI(new int[]{0, 10}, 2));
    System.out.println(SOLUTION.smallestRangeI(new int[]{1, 3, 6}, 3));
  }

  @Test
  public void test_fairCandySwap() {
    System.out.println(Arrays.toString(SOLUTION.fairCandySwap(new int[]{1, 1}, new int[]{2, 2})));
    System.out.println(Arrays.toString(SOLUTION.fairCandySwap(new int[]{1, 2}, new int[]{2, 3})));
    System.out.println(Arrays.toString(SOLUTION.fairCandySwap(new int[]{2}, new int[]{1, 3})));
    System.out.println(Arrays.toString(SOLUTION.fairCandySwap(new int[]{1, 2, 5}, new int[]{2, 4})));
  }

  @Test
  public void test_backspaceCompare() {
    System.out.println(SOLUTION.backspaceCompare("bbbextm", "bbb#extm"));
    System.out.println(SOLUTION.backspaceCompare("nzp#o#g", "b#nzp#o#g"));
    System.out.println(SOLUTION.backspaceCompare("ab#c", "ad#c"));
    System.out.println(SOLUTION.backspaceCompare("ab##", "c#d#"));
    System.out.println(SOLUTION.backspaceCompare("a#c", "b"));
  }

  @Test
  public void test_duplicateZeros() {
    int[] arr = new int[]{8, 4, 5, 0, 0, 0, 0, 7};
    SOLUTION.duplicateZeros(arr);
    System.out.println(Arrays.toString(arr));
    arr = new int[]{0, 0, 0, 0, 0, 0, 0};
    SOLUTION.duplicateZeros(arr);
    System.out.println(Arrays.toString(arr));
    arr = new int[]{0, 4, 1, 0, 0, 8, 0, 0, 3};
    SOLUTION.duplicateZeros(arr);
    System.out.println(Arrays.toString(arr));
    arr = new int[]{1, 0, 2, 3, 0, 4, 5, 0};
    SOLUTION.duplicateZeros(arr);
    System.out.println(Arrays.toString(arr));
    arr = new int[]{1, 2, 3};
    SOLUTION.duplicateZeros(arr);
    System.out.println(Arrays.toString(arr));
    arr = new int[]{1, 5, 2, 0, 6, 8, 0, 6, 0};
    SOLUTION.duplicateZeros(arr);
    System.out.println(Arrays.toString(arr));
  }

  @Test
  public void test_lastStoneWeight() {
    System.out.println(SOLUTION.lastStoneWeight(new int[]{2, 2}));
    System.out.println(SOLUTION.lastStoneWeight(new int[]{2, 7, 4, 1, 8, 1}));
  }

  @Test
  public void test_prefixesDivBy5() {
    System.out.println(SOLUTION.prefixesDivBy5(new int[]{1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0}));
    System.out.println(SOLUTION.prefixesDivBy5(new int[]{1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1}));
    System.out.println(SOLUTION.prefixesDivBy5(new int[]{0, 1, 1}));
    System.out.println(SOLUTION.prefixesDivBy5(new int[]{1, 1, 1}));
  }

  @Test
  public void test_printBin() {
    System.out.println(SOLUTION.printBin(0.078125));
    System.out.println(SOLUTION.printBin(0.625));
    System.out.println(SOLUTION.printBin(0.1));
  }

  @Test
  public void test_playQq() {
    System.out.println(Arrays.toString(SOLUTION.playQq(new int[]{6, 3, 1, 7, 5, 8, 9, 2, 4})));
  }

  @Test
  public void test_findOcurrences() {
    System.out.println(Arrays.toString(SOLUTION.findOcurrences("alice is a good girl she is a good student", "a", "good")));
    System.out.println(Arrays.toString(SOLUTION.findOcurrences("we will we will rock you", "we", "will")));
  }

  @Test
  public void test_largestLocal() {
    System.out.println(Arrays.deepToString(SOLUTION.largestLocal(new int[][]{
      new int[]{9, 9, 8, 1},
      new int[]{5, 6, 2, 6},
      new int[]{8, 2, 6, 4},
      new int[]{6, 2, 2, 2}
    })));
    System.out.println(Arrays.deepToString(SOLUTION.largestLocal(new int[][]{
      new int[]{1, 1, 1, 1, 1},
      new int[]{1, 1, 1, 1, 1},
      new int[]{1, 1, 2, 1, 1},
      new int[]{1, 1, 1, 1, 1},
      new int[]{1, 1, 1, 1, 1},
    })));
  }

  @Test
  public void test_binaryGap() {
    System.out.println(SOLUTION.binaryGap(22));
    System.out.println(SOLUTION.binaryGap(8));
    System.out.println(SOLUTION.binaryGap(5));
  }

  @Test
  public void test_isMonotonic() {
    System.out.println(SOLUTION.isMonotonic(new int[]{1, 2, 2, 3}));
    System.out.println(SOLUTION.isMonotonic(new int[]{6, 5, 4, 4}));
    System.out.println(SOLUTION.isMonotonic(new int[]{1, 3, 2}));
  }

  @Test
  public void test_transpose() {
    System.out.println(Arrays.deepToString(SOLUTION.transpose(new int[][]{
      new int[]{1, 2, 3},
      new int[]{4, 5, 6},
      new int[]{7, 8, 9}
    })));
    System.out.println(Arrays.deepToString(SOLUTION.transpose(new int[][]{
      new int[]{1, 2, 3},
      new int[]{4, 5, 6}
    })));
  }

  @Test
  public void test_largeGroupPositions() {
    System.out.println(SOLUTION.largeGroupPositions("aaa"));
    System.out.println(SOLUTION.largeGroupPositions("abbxxxxzzy"));
    System.out.println(SOLUTION.largeGroupPositions("abc"));
    System.out.println(SOLUTION.largeGroupPositions("abcdddeeeeaabbbcd"));
    System.out.println(SOLUTION.largeGroupPositions("aba"));

  }

  @Test
  public void test_mergeSimilarItems() {
    System.out.println(SOLUTION.mergeSimilarItems(buildArray(15, 5, 2, 6, 5, 3, 14, 8, 12, 4, 19, 6, 25, 4, 13, 4, 9, 1),
      buildArray(15, 9, 2, 4, 5, 2, 14, 4, 12, 3, 19, 10, 25, 7, 13, 6, 9, 9)));
    System.out.println(SOLUTION.mergeSimilarItems(buildArray(1, 1, 4, 5, 3, 8), buildArray(3, 1, 1, 5)));
    System.out.println(SOLUTION.mergeSimilarItems(buildArray(1, 1, 3, 2, 2, 3), buildArray(2, 1, 3, 2, 1, 3)));
    System.out.println(SOLUTION.mergeSimilarItems(buildArray(1, 3, 2, 2), buildArray(7, 1, 2, 2, 1, 4)));
  }

  @Test
  public void test_mostCommonWord() {
    System.out.println(SOLUTION.mostCommonWord("Bob hit a ball, the hit BALL flew far after it was hit.", new String[]{"hit"}));
    System.out.println(SOLUTION.mostCommonWord("Bob", new String[0]));
    System.out.println(SOLUTION.mostCommonWord("Bob. hIt, baLl", new String[]{"bob", "hit"}));
  }

  @Test
  public void test_buddyStrings() {
    System.out.println(SOLUTION.buddyStrings("abab", "abab"));
    System.out.println(SOLUTION.buddyStrings("ab", "ba"));
    System.out.println(SOLUTION.buddyStrings("aa", "aa"));
    System.out.println(SOLUTION.buddyStrings("abcd", "cbad"));
    System.out.println(SOLUTION.buddyStrings("ab", "ab"));
  }

  @Test
  public void test_shortestToChar() {
    System.out.println(Arrays.toString(SOLUTION.shortestToChar("loveleetcodekkk", 'e')));
    System.out.println(Arrays.toString(SOLUTION.shortestToChar("aaab", 'b')));
  }

  @Test
  public void test_minimumOperations() {
    System.out.println(SOLUTION.minimumOperations(new int[]{1, 6, 0, 3, 6}));
    System.out.println(SOLUTION.minimumOperations(new int[]{0}));
  }

  @Test
  public void test_rotateString() {
    System.out.println(SOLUTION.rotateString("clrwmpkwru", "wmpkwruclr"));
    System.out.println(SOLUTION.rotateString("abcde", "cdeab"));
    System.out.println(SOLUTION.rotateString("abcde", "abced"));
  }

  @Test
  public void test_uniqueMorseRepresentations() {
    System.out.println(SOLUTION.uniqueMorseRepresentations(new String[]{"gin", "zen", "gig", "msg"}));
    System.out.println(SOLUTION.uniqueMorseRepresentations(new String[]{"a"}));
  }

  @Test
  public void test_isToeplitzMatrix() {
    System.out.println(SOLUTION.isToeplitzMatrix(new int[][]{
      new int[]{36, 59, 71, 15, 26, 82, 87},
      new int[]{56, 36, 59, 71, 15, 26, 82},
      new int[]{15, 0, 36, 59, 71, 15, 26}
    }));
    System.out.println(SOLUTION.isToeplitzMatrix(new int[][]{
      new int[]{1, 2, 3, 4},
      new int[]{5, 1, 2, 3},
      new int[]{9, 5, 1, 2}
    }));
    System.out.println(SOLUTION.isToeplitzMatrix(new int[][]{
      new int[]{1, 2},
      new int[]{2, 2}
    }));
  }

  @Test
  public void test_countPrimeSetBits() {
    System.out.println(SOLUTION.countPrimeSetBits(6, 10));
    System.out.println(SOLUTION.countPrimeSetBits(10, 15));
  }

  @Test
  public void test_shortestCompletingWord() {
    System.out.println(SOLUTION.shortestCompletingWord("1s3 PSt", new String[]{"step", "steps", "stripe", "stepple"}));
    System.out.println(SOLUTION.shortestCompletingWord("1s3 456", new String[]{"looks", "pest", "stew", "show"}));
  }

  @Test
  public void test_dominantIndex() {
    System.out.println(SOLUTION.dominantIndex(new int[]{3, 6, 1, 0}));
    System.out.println(SOLUTION.dominantIndex(new int[]{1, 2, 3, 4}));
    System.out.println(SOLUTION.dominantIndex(new int[]{1}));
  }

  @Test
  public void test_pivotIndex() {
    System.out.println(SOLUTION.pivotIndex(new int[]{1, 7, 3, 6, 5, 6}));
    System.out.println(SOLUTION.pivotIndex(new int[]{1, 2, 3}));
    System.out.println(SOLUTION.pivotIndex(new int[]{2, 1, -1}));
  }

  @Test
  public void test_MyHashMap() {
    MyHashMap myHashMap = new MyHashMap();
    myHashMap.put(1, 1);
    myHashMap.put(2, 2);
    System.out.println(myHashMap.get(1));
    System.out.println(myHashMap.get(3));
    myHashMap.put(2, 1);
    System.out.println(myHashMap.get(2));
    myHashMap.remove(2);
    System.out.println(myHashMap.get(2));
  }

  @Test
  public void test_MyHashSet() {
    MyHashSet myHashSet = new MyHashSet();
    myHashSet.add(1);
    myHashSet.add(2);
    System.out.println(myHashSet.contains(1));
    System.out.println(myHashSet.contains(3));
    myHashSet.add(2);
    System.out.println(myHashSet.contains(2));
    myHashSet.remove(2);
    System.out.println(myHashSet.contains(2));
  }

  @Test
  public void test_isOneBitCharacter() {
    System.out.println(SOLUTION.isOneBitCharacter(new int[]{1, 0, 0}));
    System.out.println(SOLUTION.isOneBitCharacter(new int[]{1, 1, 1, 0}));
  }

  @Test
  public void test_selfDividingNumbers() {
    System.out.println(SOLUTION.selfDividingNumbers(1, 22));
    System.out.println(SOLUTION.selfDividingNumbers(47, 85));
  }

  @Test
  public void test_canPlaceFlowers() {
    System.out.println(SOLUTION.canPlaceFlowers(new int[]{0, 0, 0}, 2));
    System.out.println(SOLUTION.canPlaceFlowers(new int[]{1}, 0));
    System.out.println(SOLUTION.canPlaceFlowers(new int[]{0}, 1));
    System.out.println(SOLUTION.canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 1));
    System.out.println(SOLUTION.canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 2));
  }

  @Test
  public void test_findRestaurant() {
    System.out.println(Arrays.toString(SOLUTION.findRestaurant(new String[]{"Shogun", "Tapioca Express", "Burger King", "KFC"},
      new String[]{"Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"})));
    System.out.println(Arrays.toString(SOLUTION.findRestaurant(new String[]{"Shogun", "Tapioca Express", "Burger King", "KFC"},
      new String[]{"KFC", "Shogun", "Burger King"})));
  }

  @Test
  public void test_matrixReshape() {
    System.out.println(Arrays.deepToString(SOLUTION.matrixReshape(buildArray(1, 2, 3, 4), 1, 4)));
    System.out.println(Arrays.deepToString(SOLUTION.matrixReshape(buildArray(1, 2, 3, 4), 2, 4)));
  }

  @Test
  public void test_getMinimumDifference() {
    System.out.println(SOLUTION.getMinimumDifference(new TreeNode(4, new TreeNode(2, new TreeNode(1), new TreeNode(3)), new TreeNode(6))));
    System.out.println(SOLUTION.getMinimumDifference(new TreeNode(1, new TreeNode(0), new TreeNode(48, new TreeNode(12), new TreeNode(49)))));
  }

  @Test
  public void test_findMode() {
    System.out.println(Arrays.toString(SOLUTION.findMode(new TreeNode(1, null, new TreeNode(2, new TreeNode(2), null)))));
    System.out.println(Arrays.toString(SOLUTION.findMode(new TreeNode(0))));
  }

  @Test
  public void test_sumOfLeftLeaves() {
    System.out.println(SOLUTION.sumOfLeftLeaves(new TreeNode(1)));
    System.out.println(SOLUTION.sumOfLeftLeaves(new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)))));
  }

  @Test
  public void test_binaryTreePaths() {
    System.out.println(SOLUTION.binaryTreePaths(new TreeNode(1)));
    System.out.println(SOLUTION.binaryTreePaths(new TreeNode(1, new TreeNode(2, null, new TreeNode(5)), new TreeNode(3))));
  }

  @Test
  public void test_bestHand() {
    System.out.println(SOLUTION.bestHand(new int[]{13, 2, 3, 1, 9}, new char[]{'a', 'a', 'a', 'a', 'a'}));
    System.out.println(SOLUTION.bestHand(new int[]{4, 4, 2, 4, 4}, new char[]{'d', 'a', 'a', 'b', 'c'}));
    System.out.println(SOLUTION.bestHand(new int[]{10, 10, 2, 12, 9}, new char[]{'a', 'b', 'c', 'a', 'd'}));
  }

  @Test
  public void test_findShortestSubArray() {
    System.out.println(SOLUTION.findShortestSubArray(new int[]{1, 2, 2, 3, 1}));
    System.out.println(SOLUTION.findShortestSubArray(new int[]{1, 2, 2, 1, 2, 1, 1, 1, 1, 2, 2, 2}));
    System.out.println(SOLUTION.findShortestSubArray(new int[]{1}));
    System.out.println(SOLUTION.findShortestSubArray(new int[]{1, 3, 2, 2, 3, 1}));
    System.out.println(SOLUTION.findShortestSubArray(new int[]{1, 2, 2, 3, 1, 4, 2}));
  }

  @Test
  public void test_validPalindrome() {
    System.out.println(SOLUTION.validPalindrome(
      "aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupuculmgmqfvnbgtapekouga"));
    System.out.println(SOLUTION.validPalindrome("aba"));
    System.out.println(SOLUTION.validPalindrome("abca"));
    System.out.println(SOLUTION.validPalindrome("abc"));
  }

  @Test
  public void test_calPoints() {
    System.out.println(SOLUTION.calPoints(new String[]{"1", "C"}));
    System.out.println(SOLUTION.calPoints(new String[]{"5", "2", "C", "D", "+"}));
    System.out.println(SOLUTION.calPoints(new String[]{"5", "-2", "4", "C", "D", "9", "+", "+"}));
    System.out.println(SOLUTION.calPoints(new String[]{"1"}));
  }

  @Test
  public void test_countBinarySubstrings() {
    // for (int i = 0; i < 8; i++) {
    //   System.out.println(Arrays.toString(SOLUTION.zeroAndOneString(i)));
    // }
    System.out.println(SOLUTION.countBinarySubstrings("00110011"));
    System.out.println(SOLUTION.countBinarySubstrings("10101"));
  }

  @Test
  public void test_islandPerimeter() {
    System.out.println(SOLUTION.islandPerimeter(new int[][]{
      new int[]{0, 1, 0, 0},
      new int[]{1, 1, 1, 0},
      new int[]{0, 1, 0, 0},
      new int[]{1, 1, 0, 0},
    }));
  }

  @Test
  public void test_toLowerCase() {
    for (int i = 'A'; i <= 'z'; i++) {
      System.out.println((char) i + "\t" + b(i));
    }
    System.out.println(SOLUTION.toLowerCase("Hello"));
  }

  @Test
  public void test_findJudge() {
    System.out.println(SOLUTION.findJudge(4, buildArray(1, 3, 1, 4, 2, 3, 2, 4, 4, 3)));
    System.out.println(SOLUTION.findJudge(2, buildArray(1, 2)));
    System.out.println(SOLUTION.findJudge(3, buildArray(1, 3, 2, 3)));
    System.out.println(SOLUTION.findJudge(3, buildArray(1, 3, 2, 3, 3, 1)));
  }

  @Test
  public void test_sortArrayByParity() {
    System.out.println(Arrays.toString(SOLUTION.sortArrayByParity(new int[]{3, 1, 2, 4})));
    System.out.println(Arrays.toString(SOLUTION.sortArrayByParity(new int[]{0})));
  }

  @Test
  public void test_getLeastNumbers() {
    System.out.println(Arrays.toString(SOLUTION.getLeastNumbers(new int[]{3, 2, 1}, 2)));
    System.out.println(Arrays.toString(SOLUTION.getLeastNumbers(new int[]{0, 1, 2, 1}, 1)));
  }

  @Test
  public void test_reverseWords() {
    System.out.println(SOLUTION.reverseWords("Let's take LeetCode contest"));
    System.out.println(SOLUTION.reverseWords("God Ding"));
  }

  @Test
  public void test_getKthFromEnd() {
    System.out.println(SOLUTION.getKthFromEnd(ListNode.of(1, 2, 3, 4, 5), 2));
  }

  @Test
  public void test_sortedSquares() {
    System.out.println(Arrays.toString(SOLUTION.sortedSquares(new int[]{-4, -1, 0, 3, 10})));
    System.out.println(Arrays.toString(SOLUTION.sortedSquares(new int[]{-7, -3, 2, 3, 11})));
  }

  @Test
  public void test_search() {
    System.out.println(SOLUTION.search(new int[]{2, 5}, 5));
    System.out.println(SOLUTION.search(new int[]{-1, 0, 3, 5, 9, 12}, 9));
    System.out.println(SOLUTION.search(new int[]{-1, 0, 3, 5, 9, 12}, 2));
  }

  @Test
  public void test_numberOfPairs() {
    System.out.println(Arrays.toString(SOLUTION.numberOfPairs(new int[]{1, 3, 2, 1, 3, 2, 2})));
    System.out.println(Arrays.toString(SOLUTION.numberOfPairs(new int[]{1, 1})));
    System.out.println(Arrays.toString(SOLUTION.numberOfPairs(new int[]{0})));
  }

  @Test
  public void test_arrayPairSum() {
    System.out.println(SOLUTION.arrayPairSum(new int[]{1, 4, 3, 2}));
    System.out.println(SOLUTION.arrayPairSum(new int[]{6, 2, 6, 5, 1, 2}));
  }

  @Test
  public void test_checkRecord() {
    System.out.println(SOLUTION.checkRecord("AA"));
    System.out.println(SOLUTION.checkRecord("PPALLP"));
    System.out.println(SOLUTION.checkRecord("PPALLL"));
  }

  @Test
  public void test_longestPalindrome() {
    System.out.println(SOLUTION.longestPalindrome("bb"));
    System.out.println(SOLUTION.longestPalindrome("abccccdd"));
    System.out.println(SOLUTION.longestPalindrome("a"));
    System.out.println(SOLUTION.longestPalindrome("aaaaaccc"));
  }

  @Test
  public void test_isSubsequence() {
    String longString = "yyyyyl" +
      "yy".repeat(147) + "e" +
      "yyy".repeat(433) + "e" +
      "y".repeat(1879) + "e" +
      "yyy".repeat(2) + "e" +
      "yyyy".repeat(253) + "e" +
      "y".repeat(597) + "e" +
      "y".repeat(233) + "t" +
      "yyyyyy".repeat(41) + "c" +
      "yyy".repeat(440) + "o" +
      "yyy".repeat(333) + "d" +
      "yy".repeat(763) + "e" +
      "yyyy".repeat(118);
    System.out.println(SOLUTION.isSubsequence("leeeeetcode", longString));
    System.out.println(SOLUTION.isSubsequence("abc", "ahbgdc"));
    System.out.println(SOLUTION.isSubsequence("axc", "ahbgdc"));
  }

  @Test
  public void test_checkValid() {
    System.out.println(SOLUTION.checkValid(new int[][]{new int[]{2, 2, 2}, new int[]{2, 2, 2}, new int[]{2, 2, 2}}));
    System.out.println(SOLUTION.checkValid(new int[][]{new int[]{1, 2, 3}, new int[]{3, 1, 2}, new int[]{2, 3, 1}}));
  }

  @Test
  public void test_findWords() {
    System.out.println(Arrays.toString(SOLUTION.findWords(new String[]{"asdfghjklASDFGHJKL", "qwertyuiopQWERTYUIOP", "zxcvbnmZXCVBNM"})));
  }

  @Test
  public void test_repeatedSubstringPattern() {
    System.out.println(SOLUTION.repeatedSubstringPattern("abab"));
    System.out.println(SOLUTION.repeatedSubstringPattern("aba"));
    System.out.println(SOLUTION.repeatedSubstringPattern("abcabcabcabc"));
  }

  @Test
  public void test_addStrings() {
    System.out.println(SOLUTION.addStrings("99", "99"));
    System.out.println(SOLUTION.addStrings("11", "123"));
    System.out.println(SOLUTION.addStrings("456", "77"));
    System.out.println(SOLUTION.addStrings("0", "0"));
  }

  @Test
  public void test_MyQueue() {
    MyQueue myQueue = new MyQueue();
    myQueue.push(1);
    myQueue.push(2);
    System.out.println(myQueue.peek());
    System.out.println(myQueue.pop());
    System.out.println(myQueue.empty());
  }

  @Test
  public void test_MyStack() {
    MyStack myStack = new MyStack();
    myStack.push(1);
    myStack.push(2);
    System.out.println(myStack.top());
    System.out.println(myStack.pop());
    System.out.println(myStack.empty());
  }

  @Test
  public void test_reverseList() {
    System.out.println(SOLUTION.reverseList(ListNode.of(1, 2, 3, 4, 5)));
    System.out.println(SOLUTION.reverseList(ListNode.of(1, 2)));
    System.out.println(SOLUTION.reverseList(ListNode.of()));
  }

  @Test
  public void test_removeElements() {
    System.out.println(SOLUTION.removeElements(ListNode.of(1, 2, 2, 1), 2));
    System.out.println(SOLUTION.removeElements(ListNode.of(1, 2, 6, 3, 4, 5, 6), 6));
    System.out.println(SOLUTION.removeElements(ListNode.of(), 1));
    System.out.println(SOLUTION.removeElements(ListNode.of(7, 7, 7, 7), 7));
  }

  @Test
  public void test_fillCups() {
    System.out.println(SOLUTION.fillCups(new int[]{4, 2, 4}));
    System.out.println(SOLUTION.fillCups(new int[]{2, 4, 2}));
    System.out.println(SOLUTION.fillCups(new int[]{3, 4, 4}));
    System.out.println(SOLUTION.fillCups(new int[]{1, 4, 2}));
    System.out.println(SOLUTION.fillCups(new int[]{5, 4, 4}));
    System.out.println(SOLUTION.fillCups(new int[]{5, 0, 0}));
  }

  @Test
  public void test_containsNearbyDuplicate() {
    System.out.println(SOLUTION.containsNearbyDuplicate(new int[]{99, 99}, 2));
    System.out.println(SOLUTION.containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 3));
    System.out.println(SOLUTION.containsNearbyDuplicate(new int[]{1, 0, 1, 1}, 1));
    System.out.println(SOLUTION.containsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3}, 2));
  }

  @Test
  public void test_convertToTitle_titleToNumber() {
    // for (int i = 0; i < 8; i++) {
    //   double pow = Math.pow(26, i);
    //   System.out.println((long) pow + ": " + (pow >= Integer.MAX_VALUE));
    // }
    System.out.println(SOLUTION.titleToNumber("A"));
    System.out.println(SOLUTION.titleToNumber("AB"));
    System.out.println(SOLUTION.titleToNumber("ZY"));

    // System.out.println(SOLUTION.convertToTitle(52));
    // System.out.println(SOLUTION.convertToTitle(1));
    // System.out.println(SOLUTION.convertToTitle(28));
    // System.out.println(SOLUTION.convertToTitle(701));
    // System.out.println(SOLUTION.convertToTitle(2147483647));
  }

  @Test
  public void test_isPalindrome() {
    System.out.println(SOLUTION.isPalindrome("A man, a plan, a canal: Panama"));
    System.out.println(SOLUTION.isPalindrome("race a car"));
    System.out.println(SOLUTION.isPalindrome(" "));
  }

  @Test
  public void test_isBalanced() {
    TreeNode treeNode = new TreeNode(1, new TreeNode(2, new TreeNode(3, new TreeNode(4), new TreeNode(4)), new TreeNode(3)), new TreeNode(2));
    System.out.println(SOLUTION.isBalanced(treeNode));
  }

  @Test
  public void test_sortedArrayToBST() {
    int[] arr = new int[]{-10, -3, 0, 5, 9};
    arr = new int[]{1, 3};
    TreeNode treeNode = SOLUTION.sortedArrayToBST(arr);
    System.out.println(treeNode);
  }

  @Test
  public void test_reverseVowels() {
    System.out.println(SOLUTION.reverseVowels("hello"));
    System.out.println(SOLUTION.reverseVowels("leetcode"));
  }

  @Test
  public void test_isAnagram() {
    System.out.println(SOLUTION.isAnagram("rat", "car"));
  }

  @Test
  public void test_containsDuplicate() {
    System.out.println(SOLUTION.containsDuplicate(new int[]{1, 2, 3, 1}));
    System.out.println(SOLUTION.containsDuplicate(new int[]{1, 2, 3, 4}));
    System.out.println(SOLUTION.containsDuplicate(new int[]{1, 1, 1, 3, 3, 4, 3, 2, 4, 2}));
  }

  @Test
  public void test_isIsomorphic() {
    System.out.println(Integer.toBinaryString(Integer.MAX_VALUE));
    System.out.println(Long.toBinaryString(Long.MAX_VALUE));
    System.out.println(SOLUTION.isIsomorphic("egg", "add"));
    System.out.println(SOLUTION.isIsomorphic("foo", "bar"));
    System.out.println(SOLUTION.isIsomorphic("paper", "title"));
  }

  @Test
  public void test_reverseBits() {
    System.out.println(SOLUTION.reverseBits(-3));
    System.out.println(SOLUTION.reverseBits(43261596));
  }

  @Test
  public void test_maxDepth() {
    System.out.println(SOLUTION.maxDepth(TREE_NODE));
  }

  @Test
  public void test_detectCapitalUse() {
    System.out.println(SOLUTION.detectCapitalUse("USA"));
    System.out.println(SOLUTION.detectCapitalUse("wonderland"));
    System.out.println(SOLUTION.detectCapitalUse("Google"));
    System.out.println(SOLUTION.detectCapitalUse("FlaG"));
    System.out.println(SOLUTION.detectCapitalUse("mA"));
  }

  @Test
  public void test_findPoisonedDuration() {
    System.out.println(SOLUTION.findPoisonedDuration(new int[]{1, 2, 3, 4, 5}, 5));
    System.out.println(SOLUTION.findPoisonedDuration(new int[]{1, 4}, 2));
    System.out.println(SOLUTION.findPoisonedDuration(new int[]{1, 2}, 2));
  }

  @Test
  public void test_fib() {
    System.out.println(SOLUTION.fib(4));
  }

  @Test
  public void test_checkPerfectNumber() {
    System.out.println(SOLUTION.checkPerfectNumber(36));//1 2 3 4 6 9 12 18 36 false
    System.out.println(SOLUTION.checkPerfectNumber(28));//true
    System.out.println(SOLUTION.checkPerfectNumber(7));//false
  }

  @Test
  public void test_convertToBase7() {
    System.out.println(SOLUTION.convertToBase7(100));//202
    System.out.println(SOLUTION.convertToBase7(-7));//-10
  }

  @Test
  public void test_arrangeCoins() {
    System.out.println(SOLUTION.arrangeCoins(1804289383));//60070
    System.out.println(SOLUTION.arrangeCoins(5));//2
    System.out.println(SOLUTION.arrangeCoins(8));//3
    System.out.println(SOLUTION.arrangeCoins(10));//4
  }

  @Test
  public void test_nextGreaterElement() {
    System.out.println(Arrays.toString(SOLUTION.nextGreaterElement(new int[]{1, 3, 5, 2, 4}, new int[]{6, 5, 4, 3, 2, 1, 7})));
    System.out.println(Arrays.toString(SOLUTION.nextGreaterElement(new int[]{4, 1, 2}, new int[]{1, 3, 4, 2})));
    System.out.println(Arrays.toString(SOLUTION.nextGreaterElement(new int[]{2, 4}, new int[]{1, 2, 3, 4})));
  }

  @Test
  public void test_countSegments() {
    System.out.println(SOLUTION.countSegments(""));
    System.out.println(SOLUTION.countSegments(", , , ,        a, eaefa"));
  }

  @Test
  public void test_hammingDistance() {
    System.out.println(SOLUTION.hammingDistance(1, 4));
    System.out.println(SOLUTION.hammingDistance(3, 1));
  }

  @Test
  public void test_licenseKeyFormatting() {
    System.out.println(SOLUTION.licenseKeyFormatting("--a-a-a-a--", 2));
    System.out.println(SOLUTION.licenseKeyFormatting("5F3Z-2e-9-w", 4));
    System.out.println(SOLUTION.licenseKeyFormatting("2-5g-3-J", 2));
  }

  @Test
  public void test_findMaxConsecutiveOnes() {
    int[] arr = new int[10000];
    for (int i = 0; i < 10000; i++) {
      if (i == 1516 || i == 4357 || i == 5949 || i == 7682) {
        arr[i] = 0;
      }
      arr[i] = 1;
    }
    System.out.println(SOLUTION.findMaxConsecutiveOnes(arr));
    System.out.println(SOLUTION.findMaxConsecutiveOnes(new int[]{1, 1, 0, 1, 1, 1}));
    System.out.println(SOLUTION.findMaxConsecutiveOnes(new int[]{1, 0, 1, 1, 0, 1}));
  }

  @Test
  public void test_findContentChildren() {
    System.out.println(SOLUTION.findContentChildren(new int[]{10, 9, 8, 7}, new int[]{5, 6, 7, 8}));
    System.out.println(SOLUTION.findContentChildren(new int[]{1, 2}, new int[]{1, 2, 3}));
    System.out.println(SOLUTION.findContentChildren(new int[]{1, 2, 3}, new int[]{1, 1}));
  }

  @Test
  public void test_findDisappearedNumbers() {
    System.out.println(SOLUTION.findDisappearedNumbers(new int[]{2, 2}));
    System.out.println(SOLUTION.findDisappearedNumbers(new int[]{1, 1}));
    System.out.println(SOLUTION.findDisappearedNumbers(new int[]{4, 3, 2, 7, 8, 2, 3, 1}));
  }

  @Test
  public void test_alertNames() {
    System.out.println(SOLUTION.alertNames(new String[]{"a", "a", "a", "a", "a", "b", "b", "b", "b", "b", "b"},
      new String[]{"04:48", "23:53", "06:36", "07:45", "12:16", "00:52", "10:59", "17:16", "00:36", "01:26", "22:42"}));
    System.out.println(SOLUTION.alertNames(new String[]{"leslie", "leslie", "leslie", "clare", "clare", "clare", "clare"},
      new String[]{"13:00", "13:20", "14:00", "18:00", "18:51", "19:30", "19:49"}));
    System.out.println(SOLUTION.alertNames(new String[]{"john", "john", "john"}, new String[]{"23:58", "23:59", "00:01"}));
    System.out.println(SOLUTION.alertNames(new String[]{"daniel", "daniel", "daniel", "luis", "luis", "luis", "luis"},
      new String[]{"10:00", "10:40", "11:00", "09:00", "11:00", "13:00", "15:00"}));
  }

  @Test
  public void test_findComplement() {
    // 5->2  1->0
    System.out.println(b(5) + "\t" + b(SOLUTION.findComplement(5)));
    System.out.println(b(1) + "\t" + b(SOLUTION.findComplement(1)));
  }

  @Test
  public void test_summaryRanges() {
    System.out.println(SOLUTION.summaryRanges(new int[]{-2147483648, -2147483647, 2147483647}));
  }

  @Test
  public void test_isHappy() {
    System.out.println(SOLUTION.isHappy(19));
  }

  @Test
  public void test_majorityElement() {
    System.out.println(SOLUTION.majorityElement(new int[]{6, 5, 5}));
  }

  @Test
  public void test_reverseString() {
    char[] chars1 = "abcdefg".toCharArray();
    char[] chars2 = "abcdefgh".toCharArray();
    SOLUTION.reverseString(chars1);
    SOLUTION.reverseString(chars2);
    System.out.println(chars1);
    System.out.println(chars2);
  }

  @Test
  public void test_reverseStr() {
    System.out.println(SOLUTION.reverseStr("abcdefg", 2));
  }

  @Test
  public void test_moveZeroes() {
    int[] arr = {0, 1, 0, 3, 12};
    arr = new int[]{0};
    arr = new int[]{0, 1};
    SOLUTION.moveZeroes(arr);
    System.out.println(Arrays.toString(arr));
  }

  @Test
  public void test_firstBadVersion() {
    System.out.println(SOLUTION.firstBadVersion(3));
  }

  @Test
  public void test_isUgly() {
    System.out.println(SOLUTION.isUgly(6));//true
    System.out.println(SOLUTION.isUgly(1));//true
    System.out.println(SOLUTION.isUgly(14));//false
  }

  @Test
  public void test_thirdMax() {
    System.out.println(SOLUTION.thirdMax(new int[]{5, 2, 4, 1, 3, 6, 0}));//4
    System.out.println(SOLUTION.thirdMax(new int[]{1, 1, 2}));//2
    System.out.println(SOLUTION.thirdMax(new int[]{1, -2147483648, 2}));//-2147483648
    System.out.println(SOLUTION.thirdMax(new int[]{3, 2, 1}));//1
    System.out.println(SOLUTION.thirdMax(new int[]{1, 2}));//2
    System.out.println(SOLUTION.thirdMax(new int[]{2, 2, 3, 1}));//1
  }

  @Test
  public void test_fizzBuzz() {
    System.out.println(SOLUTION.fizzBuzz(3));
    System.out.println(SOLUTION.fizzBuzz(5));
    System.out.println(SOLUTION.fizzBuzz(15));
  }

  @Test
  public void test_wordPattern() {
    System.out.println(SOLUTION.wordPattern("abba", "dog dog dog dog"));
    System.out.println(SOLUTION.wordPattern("abba", "dog cat cat dog"));
    System.out.println(SOLUTION.wordPattern("abba", "dog cat cat fish"));
    System.out.println(SOLUTION.wordPattern("aaaa", "dog cat cat dog"));
  }

  @Test
  public void test_toHex() {
    System.out.println(SOLUTION.toHex(26));
    System.out.println(SOLUTION.toHex(-1));
  }

  @Test
  public void test_firstUniqChar() {
    System.out.println(SOLUTION.firstUniqChar("leetcode"));//0
    System.out.println(SOLUTION.firstUniqChar("loveleetcode"));//2
    System.out.println(SOLUTION.firstUniqChar("aabb"));//-1
  }

  @Test
  public void test_findTheDifference() {
    System.out.println(SOLUTION.findTheDifference("abcd", "abcde"));
    System.out.println(SOLUTION.findTheDifference("", "y"));
  }

  @Test
  public void test_readBinaryWatch() {
    System.out.println(SOLUTION.readBinaryWatch(1));
    System.out.println(SOLUTION.readBinaryWatch(9));
  }

  @Test
  public void test_isPowerOfThree() {
    System.out.println(SOLUTION.isPowerOfThree(1));//true
    System.out.println(SOLUTION.isPowerOfThree(27));//true
    System.out.println(SOLUTION.isPowerOfThree(9));//true
    System.out.println(SOLUTION.isPowerOfThree(0));//false
    System.out.println(SOLUTION.isPowerOfThree(45));//false
  }

  @Test
  public void test_intersect() {
    System.out.println(Arrays.toString(SOLUTION.intersect(new int[]{1, 2, 2, 1}, new int[]{2, 2})));
    System.out.println(Arrays.toString(SOLUTION.intersect(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4})));
  }

  @Test
  public void test_guessNumber() {
    System.out.println(SOLUTION.guessNumber(2126753390));
    System.out.println(SOLUTION.guessNumber(10));
  }

  @Test
  public void test_isPerfectSquare() {
    // 195=39*5=3*5*13
    System.out.println(SOLUTION.isPerfectSquare(808201));//899
    System.out.println(SOLUTION.isPerfectSquare(2147483647));//46340.9500010519853390887900102
    System.out.println(SOLUTION.isPerfectSquare(16));
    System.out.println(SOLUTION.isPerfectSquare(14));
  }

  @Test
  public void test_decodeMessage() {
    System.out.println(SOLUTION.decodeMessage("happy boy", "vkbs bs t suepuv"));
    System.out.println(SOLUTION.decodeMessage("the quick brown fox jumps over the lazy dog", "vkbs bs t suepuv"));
  }

  @Test
  public void test_intersection() {
    System.out.println(Arrays.toString(SOLUTION.intersection(new int[]{1, 2, 2, 1}, new int[]{2, 2})));
    System.out.println(Arrays.toString(SOLUTION.intersection(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4})));
  }

  @Test
  public void test_missingNumber() {
    System.out.println(SOLUTION.missingNumber(new int[]{3, 0, 1})); //2
    System.out.println(SOLUTION.missingNumber(new int[]{0, 1})); // 2
    System.out.println(SOLUTION.missingNumber(new int[]{9, 6, 4, 2, 3, 5, 7, 0, 1})); // 8
    System.out.println(SOLUTION.missingNumber(new int[]{0})); // 1
  }

  @Test
  public void test_maxProfit() {
    System.out.println(SOLUTION.maxProfit(new int[]{1, 2}));
    System.out.println(SOLUTION.maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
    System.out.println(SOLUTION.maxProfit(new int[]{7, 6, 4, 3, 1}));
  }

  @Test
  public void test_isSameTree() {
    TreeNode t4 = new TreeNode(4);
    TreeNode t5 = new TreeNode(5);
    TreeNode t6 = new TreeNode(6);
    TreeNode t7 = new TreeNode(7);
    TreeNode left = new TreeNode(2, t7, new TreeNode(9, null, t4));
    TreeNode right = new TreeNode(3, t6, new TreeNode(88, t5, null));
    TreeNode tnode = new TreeNode(1, left, right);
    System.out.println(SOLUTION.isSameTree(TREE_NODE, tnode));
  }

  @Test
  public void test_inorderTraversal() {
    System.out.println(SOLUTION.inorderTraversal(TREE_NODE));
  }

  @Test
  public void test_countBits() {
    int range = (int) 1e5;
    System.out.println(Arrays.toString(SOLUTION.countBits(range, System.currentTimeMillis())));
    System.out.println(Arrays.toString(SOLUTION.countBits2(range, System.currentTimeMillis())));
  }

  @Test
  public void test_checkXMatrix() {
    System.out.println(SOLUTION.checkXMatrix(new int[][]{
      new int[]{2, 0, 0, 1},
      new int[]{0, 3, 1, 0},
      new int[]{0, 5, 2, 0},
      new int[]{4, 0, 0, 2}
    }));
  }

  @Test
  public void test_getSmallestString() {
    System.out.println(SOLUTION.getSmallestString(5, 130)); // zzzzz
    System.out.println(SOLUTION.getSmallestString(3, 27)); // aay
    System.out.println(SOLUTION.getSmallestString(5, 73)); // aaszz
  }

  @Test
  public void test_greatestLetter() {
    System.out.println(SOLUTION.greatestLetter("AbCdEfGhIjK"));
    System.out.println(SOLUTION.greatestLetter("arRAzFif"));
    System.out.println(SOLUTION.greatestLetter("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
    System.out.println(SOLUTION.greatestLetter("lEeTcOdE"));
    System.out.println(SOLUTION.greatestLetter("AbCdEfGhIjK"));
  }

  @Test
  public void test_countAsterisks() {
    String s = "l|*e*et|c**o|*de|";
    System.out.println(SOLUTION.countAsterisks(s));
  }

  @Test
  public void test_mergeInBetween() {
    ListNode l1 = new ListNode(0, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))));
    ListNode l2 = new ListNode(1000000, new ListNode(1000001, new ListNode(1000002)));
    System.out.println("l1 = " + l1);
    System.out.println("l2 = " + l2);
    System.out.println("SOLUTION.mergeInBetween() = " + SOLUTION.mergeInBetween(l1, 0, 4, l2));
  }

  @Test
  public void test_getRow() {
    System.out.println(SOLUTION.getRow(18));
    System.out.println(SOLUTION.getRow(0));
    System.out.println(SOLUTION.getRow(1));
    System.out.println(SOLUTION.getRow(2));
    System.out.println(SOLUTION.getRow(3));
    System.out.println(SOLUTION.getRow(4));
  }

  @Test
  public void test_generate() {
    SOLUTION.generate(5).forEach(System.out::println);
  }

  @Test
  public void test_merge() {
    int[] ints = new int[]{1, 2, 3, 0, 0, 0};
    SOLUTION.merge(ints, 3, new int[]{2, 5, 6}, 3);
    System.out.println(Arrays.toString(ints));
    System.out.println("====================================================");
    ints = new int[]{2, 0};
    SOLUTION.merge(ints, 1, new int[]{1}, 1);
    System.out.println(Arrays.toString(ints));
    System.out.println("====================================================");
    ints = new int[]{0};
    SOLUTION.merge(ints, 0, new int[]{1}, 1);
    System.out.println(Arrays.toString(ints));
    System.out.println("====================================================");
  }

  @Test
  public void test_deleteDuplicates() {
    ListNode listNode = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(3)))));
    ListNode listNode1 = SOLUTION.deleteDuplicates(listNode);
    System.out.println(listNode1);
  }

  @Test
  public void test_climbStairs() {
    System.out.println(SOLUTION.climbStairs(4));
    System.out.println(SOLUTION.climbStairs(45));
  }

  @Test
  public void test_mySqrt() {
    System.out.println(SOLUTION.mySqrt(6));
  }

  @Test
  public void test_findingUsersActiveMinutes() {
    int[][] ints = buildArray(1, 1, 2, 2, 2, 3);
    System.out.println(Arrays.toString(SOLUTION.findingUsersActiveMinutes(ints, 4)));
  }

  @Test
  public void test_strongPasswordCheckerII() {
    // "-Aa1a1a1"
    System.out.println(SOLUTION.strongPasswordCheckerII("-Aa1a1a1"));
  }

  @Test
  public void test_minMaxGame() {
    System.out.println(SOLUTION.minMaxGame(new int[]{1, 3, 5, 2, 4, 8, 2, 2}));
    System.out.println(SOLUTION.minMaxGame(new int[]{3}));
  }

  @Test
  public void test_rearrangeCharacters() {
    System.out.println("SOLUTION.rearrangeCharacters(\"ilovecodingonleetcode\", \"code\") = "
      + SOLUTION.rearrangeCharacters("ilovecodingonleetcode", "code"));
    System.out.println("SOLUTION.rearrangeCharacters(\"abcba\", \"abc\") = "
      + SOLUTION.rearrangeCharacters("abcba", "abc"));
    System.out.println("SOLUTION.rearrangeCharacters(\"abbaccaddaeea\", \"aaaaa\") = "
      + SOLUTION.rearrangeCharacters("abbaccaddaeea", "aaaaa"));
  }

  @Test
  public void test_evaluate() {
    System.out.println(SOLUTION.evaluate("(name)is(age)yearsold", buildList("name", "bob", "age", "two")));
    System.out.println(SOLUTION.evaluate("hi(name)", buildList("a", "b")));
    System.out.println(SOLUTION.evaluate("(a)(a)(a)aaa", buildList("a", "yes")));
  }

  @Test
  public void test_digitCount() {
    boolean b = SOLUTION.digitCount("030");
    System.out.println("b = " + b);
  }

  @Test
  public void test_maximumUnits() {
    System.out.println(SOLUTION.maximumUnits(new int[][]{new int[]{1, 3}, new int[]{2, 2}, new int[]{3, 1}}, 4));
  }

  @Test
  public void test_countConsistentStrings() {
    System.out.println(SOLUTION.countConsistentStrings("cad", new String[]{"cc", "acd", "b", "ba", "bac", "bad", "ac", "d"}));
  }

  @Test
  public void test_isFlipedString() {
    System.out.println(SOLUTION.isFlipedString("waterbottle", "erbottlewat"));
    System.out.println(SOLUTION.isFlipedString("aa", "aba"));
  }

  @Test
  public void test_missingTwo() {
    // System.out.println(Arrays.toString(missingTwo(new int[]{1})));
    // System.out.println(Arrays.toString(missingTwo(new int[]{2, 3})));
    // 1 2 3 4 5 6
    System.out.println(Arrays.toString(SOLUTION.missingTwo(new int[]{1, 3, 4, 6})));
  }

  @Test
  public void test_checkPermutation() {
    System.out.println(SOLUTION.checkPermutation("a", "ab"));
    System.out.println(SOLUTION.checkPermutation("abc", "bca"));
    System.out.println(SOLUTION.checkPermutation("abc", "bad"));
    System.out.println(SOLUTION.checkPermutation("abb", "aab"));
  }

  @Test
  public void test_plusOne() {
    System.out.println(Arrays.toString(SOLUTION.plusOne(new int[]{0})));
    System.out.println(Arrays.toString(SOLUTION.plusOne(new int[]{9, 9})));
    System.out.println(Arrays.toString(SOLUTION.plusOne(new int[]{1, 2, 3})));
    System.out.println(Arrays.toString(SOLUTION.plusOne(new int[]{4, 3, 2, 1})));
  }

  @Test
  public void test_lengthOfLastWord() {
    System.out.println(SOLUTION.lengthOfLastWord("Hello World"));
    System.out.println(SOLUTION.lengthOfLastWord("   fly me   to   the moon  "));
    System.out.println(SOLUTION.lengthOfLastWord("luffy is still joyboy"));
  }

  @Test
  public void test_searchInsert() {
    System.out.println(SOLUTION.searchInsert(new int[]{1, 3, 5, 6}, 5));//2
    System.out.println(SOLUTION.searchInsert(new int[]{1, 3, 5, 6}, 2));//1
    System.out.println(SOLUTION.searchInsert(new int[]{1, 3, 5, 6}, 7));//4
  }

  @Test
  public void test_frequencySort() {
    System.out.println(Arrays.toString(SOLUTION.frequencySort(new int[]{1, 1, 2, 2, 2, 3})));
  }

  @Test
  public void test_flipLights() {
    System.out.println(SOLUTION.flipLights(2, 2));
  }

  @Test
  public void test_findMedianSortedArrays() {
    System.out.println(SOLUTION.findMedianSortedArrays(new int[]{2, 2, 4, 4}, new int[]{2, 2, 4, 4}));
  }

  @Test
  public void test_trimMean() {
    int[] arr = new int[40];
    for (int i = 1; i <= 40; i++) {
      arr[i - 1] = i;
    }
    System.out.println(SOLUTION.trimMean(arr));
  }

  @Test
  public void test_maxIncreaseKeepingSkyline() {
    int[] row1 = new int[]{3, 0, 8, 4};
    int[] row2 = new int[]{2, 4, 5, 7};
    int[] row3 = new int[]{9, 2, 6, 3};
    int[] row4 = new int[]{0, 3, 1, 0};
    Assert.assertEquals(35, SOLUTION.maxIncreaseKeepingSkyline(new int[][]{row1, row2, row3, row4}));
  }

  @Test
  public void test_maximumSwap() {
    // 1993 9913
    int actual = SOLUTION.maximumSwap(98368);
    Assert.assertEquals(98863, actual);
  }

  private int figureDoubleArea(int[] arr1, int[] arr2) {
    int x1 = (arr1[2] - arr1[0]);
    int x2 = (arr2[2] - arr2[0]);
    int a = x1 + x2 - (Math.max(arr1[2], arr2[2]) - Math.min(arr1[0], arr2[0]));

    int y1 = (arr1[3] - arr1[1]);
    int y2 = (arr2[3] - arr2[1]);
    int b = y1 + y2 - (Math.max(arr1[3], arr2[3]) - Math.min(arr1[1], arr2[1]));
    return (x1 * y1 + x2 * y2) - (a * b);
  }
}
