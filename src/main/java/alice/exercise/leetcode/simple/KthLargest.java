package alice.exercise.leetcode.simple;

import java.util.PriorityQueue;

/**
 * 703. 数据流中的第 K 大元素
 *
 * @author Alice
 * @since 2023-03-17 星期五 15:09
 */
public class KthLargest {
  private final PriorityQueue<Integer> queue;
  private final int k;

  public KthLargest(int k, int[] nums) {
    queue = new PriorityQueue<>(k);
    this.k = k;
    for (int n : nums) {
      add(n);
    }
  }

  public int add(int val) {
    queue.offer(val);
    if (queue.size() > k) {
      queue.poll();
    }
    return queue.peek();
  }
}
