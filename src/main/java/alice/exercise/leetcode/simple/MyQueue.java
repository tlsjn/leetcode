package alice.exercise.leetcode.simple;

import java.util.LinkedList;
import java.util.List;

/**
 * 232. 用栈实现队列
 *
 * @author Alice
 * @since 2023-02-14 星期二 16:13
 */
class MyQueue {
  private final List<Integer> list;
  private int size;
  private int startIndex;

  public MyQueue() {
    list = new LinkedList<>();
    size = 0;
    startIndex = 0;
  }

  public void push(int x) {
    list.add(x);
    size++;
  }

  public int pop() {
    return list.get(startIndex++);
  }

  public int peek() {
    return list.get(startIndex);
  }

  public boolean empty() {
    return startIndex >= size;
  }
}
